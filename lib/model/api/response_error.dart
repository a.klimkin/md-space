import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';

const kUnknownErrorMessage = 'something_went_wrong.';

enum ErrorType {
  error,
  authError
}

class ResponseError {
  final String? message;
  final String? code;
  final ErrorType? eventType;
  

  const ResponseError._({
    this.message,
    this.code,
    this.eventType,
  });

  const ResponseError.unknown([String code = kUnknownErrorMessage])
      : this._(
    code: code,
    message: 'unknown',
    eventType: ErrorType.error,
  );

  factory ResponseError.fromError(FirebaseException error) {
    if (error is FirebaseAuthException) {
      return ResponseError._(
        code: error.code,
        message: error.message,
        eventType: ErrorType.authError,
      );
    }

    return ResponseError._(
      code: error.code,
      message: error.message,
      eventType: ErrorType.error,
    );
  }
}
