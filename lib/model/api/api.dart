import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';

import 'response.dart';
import 'response_error.dart';

class Api {

  Future<Response<T?>> get<T>(
      CollectionReference collection, {
        String? path,
        required Future<T> Function(Map<String, dynamic>) deserialize,
      }) async {

    if (path == null && T is List) {
      return _execute(() async {
        final doc = await collection.get();
        return deserialize(doc.docs.map((e) => e.data()) as Map<String, dynamic>);
      });
    }
    return _execute(() async {
      final doc = await collection.doc(path).get();
      return deserialize(doc.data()! as Map<String, dynamic>);
    });
  }

  Future<void> post<T>(
      CollectionReference collection, {
        required String path,
        Map<String, dynamic>? data,
      }) async {

     _execute(() async => collection.doc(path).set(data));
  }

  Future<Response<T?>> _execute<T>(Future<T> Function() responseFeature) async {
    try {
      final dataModel = await responseFeature();
      return Response.succeed(dataModel);
    } on FirebaseException catch (firebaseError) {
      final apiError = ResponseError.fromError(firebaseError);
      return Response.failed(apiError);
      // ignore: avoid_catching_errors
    } on Error catch (_) {
      return const Response.unknownError();
    } on Exception catch (_) {
      return const Response.unknownError();
    }
  }

  Future<Response<T?>> executeAuth<T>({required Future<T> Function() execute}) async => _execute(execute);
}