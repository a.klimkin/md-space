import 'package:flutter/foundation.dart';

import 'response_error.dart';

@immutable
class Response<T> {
  final T? data;
  final ResponseError? error;

  bool get isSucceed => error == null;

  const Response._(this.data, this.error);

  const Response.succeed(T data) : this._(data, null);

  const Response.failed(ResponseError error) : this._(null, error);

  const Response.unknownError() : this._(null, const ResponseError.unknown());
}
