import 'package:cloud_firestore/cloud_firestore.dart';

extension QueryDataObjectToMap on QueryDocumentSnapshot {
  Map<String, dynamic> get mapData => data()! as Map<String, dynamic>;
}