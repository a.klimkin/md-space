import 'package:flutter/material.dart';
import 'package:mdspace/model/booked_date.dart';

class TableModel {
  final String id;
  final String roomId;
  final String? workerId;
  final Offset position;
  final Size size;
  final int rotate;
  final List<BookedDate>? bookedDates;
  // final String? type;
  // todo add work place items in future
  // final List<String> items;

  const TableModel({
    required this.id,
    required this.roomId,
    required this.position,
    required this.size,
    required this.rotate,
    this.workerId,
    this.bookedDates,
    // this.type,
    // required this.items,
  });

  // Map<String, dynamic> get toMap => {
  //   'id': id,
  //   'roomId': roomId,
  //   'workerId': workerId,
  //   'position': position,
  //   'size': size,
  //   // 'type': type,
  //   // 'items': items,
  // };


  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TableModel &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          roomId == other.roomId;

  @override
  int get hashCode => id.hashCode ^ roomId.hashCode;
}
