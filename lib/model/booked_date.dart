class BookedDate {
  final DateTime date;
  final String uid;

  BookedDate({
    required this.date,
    required this.uid,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BookedDate && runtimeType == other.runtimeType && date == other.date && uid == other.uid;

  @override
  int get hashCode => date.hashCode ^ uid.hashCode;
}
