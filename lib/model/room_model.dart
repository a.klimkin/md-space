import 'dart:ui';

class RoomModel {
  final String id;
  final String roomName;
  final Size size;

  const RoomModel({
    required this.id,
    required this.roomName,
    required this.size,
  });

  Map<String, dynamic> get toMap => {
    'id': id,
    'roomName': roomName,
    'size': size,
  };


  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is RoomModel && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;
}