class ItemModel {
  final String id;
  final String type;
  final String name;

  const ItemModel({
    required this.id,
    required this.name,
    required this.type,
  });

  Map<String, dynamic> get toMap => {
    'id' : id,
    'name': name,
    'type': type,
  };

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ItemModel && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;
}
