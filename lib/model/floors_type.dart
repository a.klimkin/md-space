enum FloorType { underground, first, second }

extension FloorValues on FloorType {
  int get level {
    switch (this) {
      case FloorType.underground:
        return 0;
      case FloorType.first:
        return 1;
      case FloorType.second:
        return 2;
      default:
        return -1;
    }
  }

  String get title {
    switch (this) {
      case FloorType.underground:
        return "Underground floor";
      case FloorType.first:
        return "1st floor";
      case FloorType.second:
        return "2nd floor";
      default:
        return "Select floor";
    }
  }
}
