enum UserRole { user, admin, undefined }

extension UserRoleValue on UserRole {
  String get value {
    switch (this) {
      case UserRole.user:
        return 'user';
      case UserRole.admin:
        return 'admin';
      default:
        return 'undefined';
    }
  }
}

class UserModel {
  final String uid;
  final String firstName;
  final String lastName;
  final String email;
  // final String position;
  // final String department;
  // final String photoURL;
  // final String fmcToken;
  // final UserRole role;

  const UserModel({
    required this.uid,
    required this.firstName,
    required this.lastName,
    required this.email,
    // required this.role,
    // required this.position,
    // required this.department,
    // required this.photoURL,
    // required this.fmcToken,
  });

  Map<String, dynamic> get toMap => {
        'firstName': firstName,
        'lastName': lastName,
        'email': email,
        // 'position': position,
        // 'department': department,
        // 'role': role.value,
        // 'photoURL': photoURL,
        // 'fmcToken': fmcToken,
      };

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserModel && runtimeType == other.runtimeType && uid == other.uid;

  @override
  int get hashCode => uid.hashCode;
}
