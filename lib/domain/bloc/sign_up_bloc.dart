import 'dart:async';

import 'package:async/async.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:reactive_validator/reactive_validator.dart';
import 'package:rxdart/rxdart.dart';

import '../../model/api/response.dart';
import '../repository/auth_repository.dart';
import '../repository/user_repository.dart';
import '_base_bloc.dart';

class SignUpBloc extends BlocBase<SignUpState> {
  late final AuthRepository _authRepository;
  late final UserRepository _userRepository;

  @override
  StreamController<SignUpState> get streamController => _stateSubj;
  final BehaviorSubject<SignUpState> _stateSubj = BehaviorSubject<SignUpState>();
  CancelableOperation<Response<UserCredential?>>? _currentFetch;

  final StreamValidationController<SignUpFieldType> _validationController =
  SubjectStreamValidationController<SignUpFieldType>();

  SignUpState get currentState => _signInState;

  late StreamSubscription<SignUpState> _signInStateSubs;
  SignUpState _signInState = const SignUpState.empty();

  @override
  void Function(SignUpState state) get emit => _stateSubj.sink.add;

  @override
  Stream<SignUpState> get outStream => _stateSubj.stream;

  SignUpBloc({
    required AuthRepository authRepository,
    required UserRepository userRepository,
  }) :  _authRepository = authRepository,
        _userRepository= userRepository {
    _signInStateSubs = outStream.listen((event) {
      _signInState = event;
    });

    _validationController.attachConnectors([
      StreamValidationConnector<SignUpFieldType, String>(
        field: SignUpFieldType.email,
        stream: outStream.map((state) => state.email).distinct(),
        validator: const EmailValidator(),
      ),
      StreamValidationConnector<SignUpFieldType, String>(
        field: SignUpFieldType.password,
        stream: outStream.map((state) => state.password).distinct(),
        validator: const NotEmptyStringValidator(),
      ),
    ]);
  }

  @override
  void dispose() {
    _currentFetch?.cancel();
    _signInStateSubs.cancel();
    _stateSubj.close();
  }

  Future<void> signUp() async {
    emit(currentState.copyWith(status: SignUpStatus.loading));

    _currentFetch = CancelableOperation.fromFuture(
      _authRepository.createUserWithEmailAndPassword(email: currentState.email, password: currentState.password),
    );

    final Response<UserCredential?>? response =
    await _currentFetch?.valueOrCancellation(const Response.unknownError());

    if (response!.isSucceed) {
      emit(currentState.copyWith(status: SignUpStatus.authorized));
    }

  }

  void updateFormData({
    required SignUpFieldType fieldType,
    required String newValue,
  }) {
    switch (fieldType) {
      case SignUpFieldType.email:
        emit(_signInState.copyWith(email: newValue));
        break;
      case SignUpFieldType.password:
        emit(_signInState.copyWith(password: newValue));
        break;
    }
  }

  Stream<bool> fieldValidationErrorStream({
    required SignUpFieldType fieldType,
  }) =>
      _validationController
          .fieldErrorStream(fieldType)
          .map((event) => event?.isNotEmpty ?? false);

  Stream<String?> validationErrorStream({
    required SignUpFieldType fieldType,
  }) => _validationController.fieldErrorStream(fieldType);

  Future<bool> validateFields() async {
    await _validationController.validate();
    return _validationController.isValid;
  }


}

enum SignUpStatus { authorized, unauthorized, error, unknownError, loading }

class SignUpState {
  final SignUpStatus status;
  final String? errorMessage;
  final String email;
  final String password;

  const SignUpState._({
    this.status = SignUpStatus.unauthorized,
    this.errorMessage,
    this.email = '',
    this.password = '',
  });

  const SignUpState.empty() : this._();

  SignUpState copyWith({
    SignUpStatus? status,
    String? email,
    String? password,
    String? errorMessage,
  }) =>
      SignUpState._(
        status: status ?? this.status,
        email: email ?? this.email,
        password: password ?? this.password,
        errorMessage: errorMessage,
      );
}

enum SignUpFieldType {
  email,
  password,
}
