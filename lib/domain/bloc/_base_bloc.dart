import 'dart:async';

abstract class BlocBase<State> {

  void Function(State state) get emit => streamController.safeAdd;

  Stream<State> get outStream => streamController.stream;

  void dispose();

  StreamController<State> get streamController;
}

extension _EventSinkExtension<T> on StreamController<T> {
  void safeAdd(T event) {
    Future.delayed(Duration.zero, () {
      if (!isClosed) {
        sink.add(event);
      }
    });
  }
}
