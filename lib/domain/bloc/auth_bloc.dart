// import 'dart:async';
//
// import 'package:async/async.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:rxdart/rxdart.dart';
//
// import '../model/api/api_error.dart';
// import '../model/api/response.dart';
// import '../model/auth_session.dart';
// import '../repository/auth_repository.dart';
// import '_base_bloc.dart';
//
// class AuthBloc extends BlocBase<AuthState> {
//   final AuthRepository _authRepository;
//   final BehaviorSubject<AuthState> _authStateSubj = BehaviorSubject<AuthState>();
//   CancelableOperation<Response<AuthSession>> _currentFetch;
//
//   AuthBloc({
//     @required AuthRepository repository,
//   })  : _authRepository = repository;
//
//   @override
//   AuthState get currentState => _authStateSubj.stream.value;
//
//   @override
//   void Function(AuthState state) get emit => _authStateSubj.sink.add;
//
//   @override
//   Stream<AuthState> get outStream => _authStateSubj.stream;
//
//   @override
//   void dispose() {
//     _currentFetch?.cancel();
//     _authStateSubj.close();
//   }
//
//   void reset() => emit(const AuthState._(status: AuthStateStatus.newUser));
//
//   Future<void> initAuthSession() async {
//     emit(const AuthState._(status: AuthStateStatus.loadingCredentials));
//     final AuthCredentials credentials = await _authRepository.getStoredAuthCredentials();
//
//     if (credentials.token.isEmpty) {
//       Future.delayed(const Duration(milliseconds: 1000), () {
//         emit(const AuthState._(status: AuthStateStatus.newUser));
//       });
//     } else {
//       Future.delayed(const Duration(milliseconds: 1000), () {
//         emit(const AuthState._(status: AuthStateStatus.authorized));
//       });
//     }
//   }
//
//   void logout(AuthSession user) {
//     _currentFetch?.cancel();
//     emit(AuthState._(
//       status: AuthStateStatus.logoutInProgress,
//       authSession: user,
//     ));
//
//     reset();
//   }
// }
//
// enum AuthStateStatus {
//   unknown,
//   loadingCredentials,
//   logoutInProgress,
//   authorized,
//   newUser,
// }
//
// class AuthState {
//   final AuthStateStatus status;
//   final AuthSession authSession;
//   final ApiError error;
//
//   const AuthState._({
//     @required this.status,
//     this.authSession,
//     this.error,
//   });
//
//   const AuthState.unknown()
//       : this._(
//     status: AuthStateStatus.unknown,
//   );
//
//   const AuthState.authorized(AuthSession session)
//       : this._(
//     status: AuthStateStatus.authorized,
//     authSession: session,
//   );
// }
