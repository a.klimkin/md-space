import 'dart:async';

import 'package:async/async.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:reactive_validator/reactive_validator.dart';
import 'package:rxdart/rxdart.dart';

import '../../model/api/response.dart';
import '../repository/auth_repository.dart';
import '../repository/user_repository.dart';
import '_base_bloc.dart';

class SignInBloc extends BlocBase<SignInState> {
  late final AuthRepository _authRepository;
  late final UserRepository _userRepository;

  @override
  StreamController<SignInState> get streamController => _stateSubj;
  final BehaviorSubject<SignInState> _stateSubj = BehaviorSubject<SignInState>();
  CancelableOperation<Response<UserCredential?>>? _currentFetch;

  final StreamValidationController<LoginFieldType> _validationController =
  SubjectStreamValidationController<LoginFieldType>();

  SignInState get currentState => _signInState;

  late StreamSubscription<SignInState> _signInStateSubs;
  SignInState _signInState = const SignInState.empty();

  @override
  void Function(SignInState state) get emit => _stateSubj.sink.add;

  @override
  Stream<SignInState> get outStream => _stateSubj.stream;

  SignInBloc({
    required AuthRepository authRepository,
    required UserRepository userRepository,
  }) :  _authRepository = authRepository,
        _userRepository= userRepository {
    _signInStateSubs = outStream.listen((event) {
      _signInState = event;
    });

    _validationController.attachConnectors([
      StreamValidationConnector<LoginFieldType, String>(
        field: LoginFieldType.email,
        stream: outStream.map((state) => state.email).distinct(),
        validator: const EmailValidator(),
      ),
      StreamValidationConnector<LoginFieldType, String>(
        field: LoginFieldType.password,
        stream: outStream.map((state) => state.password).distinct(),
        validator: const NotEmptyStringValidator(),
      ),
    ]);
  }

  @override
  void dispose() {
    _currentFetch?.cancel();
    _signInStateSubs.cancel();
    _stateSubj.close();
  }

  Future<void> signIn() async {
    emit(currentState.copyWith(status: SignInStatus.loading));

    _currentFetch = CancelableOperation.fromFuture(
      _authRepository.signInWithEmailAndPassword(email: currentState.email, password: currentState.password),
    );

    final Response<UserCredential?>? response =
          await _currentFetch?.valueOrCancellation(const Response.unknownError());

    if (response!.isSucceed) {
      emit(currentState.copyWith(status: SignInStatus.authorized));
    }

  }

  void updateFormData({
    required LoginFieldType fieldType,
    required String newValue,
  }) {
    switch (fieldType) {
      case LoginFieldType.email:
        emit(_signInState.copyWith(email: newValue));
        break;
      case LoginFieldType.password:
        emit(_signInState.copyWith(password: newValue));
        break;
    }
  }

  Stream<bool> fieldValidationErrorStream({
    required LoginFieldType fieldType,
  }) =>
      _validationController
          .fieldErrorStream(fieldType)
          .map((event) => event?.isNotEmpty ?? false);

  Stream<String?> validationErrorStream({
    required LoginFieldType fieldType,
  }) => _validationController.fieldErrorStream(fieldType);

  Future<bool> validateFields() async {
    await _validationController.validate();
    return _validationController.isValid;
  }


}

enum SignInStatus { authorized, unauthorized, error, unknownError, loading }

class SignInState {
  final SignInStatus status;
  final String? errorMessage;
  final String email;
  final String password;

  const SignInState._({
    this.status = SignInStatus.unauthorized,
    this.errorMessage,
    this.email = '',
    this.password = '',
  });

  const SignInState.empty() : this._();

  SignInState copyWith({
    SignInStatus? status,
    String? email,
    String? password,
    String? errorMessage,
  }) =>
      SignInState._(
        status: status ?? this.status,
        email: email ?? this.email,
        password: password ?? this.password,
        errorMessage: errorMessage,
      );
}

enum LoginFieldType {
  email,
  password,
}
