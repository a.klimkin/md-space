import 'dart:async';

import 'package:async/async.dart';
import 'package:rxdart/rxdart.dart';

import '../../model/api/response.dart';
import '../../model/room_model.dart';
import '../../model/table_model.dart';
import '../repository/table_repository.dart';
import '_base_bloc.dart';

class WorkPlaceBloc extends BlocBase<PlaceState> {
  final TableRepository _repository;

  @override
  StreamController<PlaceState> get streamController => _stateSubj;
  final BehaviorSubject<PlaceState> _stateSubj = BehaviorSubject<PlaceState>();
  CancelableOperation<Response<dynamic>>? _currentFetch;

  PlaceState get currentState => _currentState;

  late StreamSubscription<PlaceState> _currentStateSubs;
  late PlaceState _currentState = const PlaceState._();

  @override
  void Function(PlaceState state) get emit => _stateSubj.sink.add;

  @override
  Stream<PlaceState> get outStream => _stateSubj.stream;

  final RoomModel _roomModel;
  RoomModel get room => _roomModel;

  WorkPlaceBloc({
    required TableRepository repository,
    required RoomModel room,
  }) :  _repository = repository,
        _roomModel = room {
    _currentStateSubs = outStream.listen((event) {
      _currentState = event;
    });
  }

  @override
  void dispose() {
    _currentFetch?.cancel();
    _currentStateSubs.cancel();
    _stateSubj.close();
  }

  void selectTable(TableModel? selectedPlace) => emit(currentState.copyWith(selectedPlace: selectedPlace));

  void resetTable() =>  emit(PlaceState._(
    status: PlaceStatus.available,
    tables: currentState.tables,
    selectedDate: currentState.selectedDate,
  ));

  bool get isTableSelected => currentState.selectedPlace != null;

  bool get isDateSelected => currentState.selectedDate != null;

  void setDate(DateTime? date) => emit(currentState.copyWith(selectedDate: date));

  Future<void>  uploadTables() async {
    emit(currentState.copyWith(status: PlaceStatus.loading));

    _currentFetch = CancelableOperation.fromFuture(
      _repository.getTables(),
    );

    final response = await _currentFetch?.valueOrCancellation(const Response.unknownError());

    if (!response!.isSucceed) {
      emit(currentState.copyWith(status: PlaceStatus.error));
    } else {
      emit(currentState.copyWith(
        status: PlaceStatus.available,
        tables: response.data as List<TableModel>,
      ));
    }

  }

  Future<void> bookTable() async {
    emit(currentState.copyWith(status: PlaceStatus.booking));
    if (!isDateSelected) {
      emit(currentState.copyWith(status: PlaceStatus.error));
    }

    _currentFetch = CancelableOperation.fromFuture(
      _repository.bookTable(tableId: currentState.selectedPlace!.id, bookedData: currentState.selectedDate!),
    );

    final response =
        await _currentFetch?.valueOrCancellation(const Response.unknownError());

    if (!response!.isSucceed) {
      emit(currentState.copyWith(status: PlaceStatus.error));
    } else {
      emit(currentState.copyWith(status: PlaceStatus.bookingSuccess));
    }
  }
}

enum PlaceStatus { unknown, available, selected, loading, booking, bookingSuccess, error }

class PlaceState {
  final PlaceStatus status;
  final List<TableModel> tables;
  final TableModel? selectedPlace;
  final DateTime? selectedDate;

  const PlaceState._({
    this.status = PlaceStatus.unknown,
    this.tables = const [],
    this.selectedPlace,
    this.selectedDate,
  });

  PlaceState copyWith({
    PlaceStatus? status,
    RoomModel? roomModel,
    List<TableModel>? tables,
    TableModel? selectedPlace,
    DateTime? selectedDate,
  }) =>
      PlaceState._(
        status: status ?? this.status,
        tables: tables ?? this.tables,
        selectedPlace: selectedPlace ?? this.selectedPlace,
        selectedDate: selectedDate ?? this.selectedDate,
      );
}
