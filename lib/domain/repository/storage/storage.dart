import 'package:shared_preferences/shared_preferences.dart';

bool isTypeOf<ThisType, OfType>() => _Instance<ThisType>() is _Instance<OfType>;

class _Instance<T> {}

enum StoredKey {
  authSession,
  authToken,
  rawSearchHistory,
}

extension _StoredKeyExt on StoredKey {
  String get rawKey => '$this';
}

class PreferencesStorage {
  PreferencesStorage();

  Future<SharedPreferences> get _prefs async => SharedPreferences.getInstance();

  Future<void> clear(StoredKey key) async {
    final prefs = await _prefs;
    await prefs.remove(key.rawKey);
  }

  Future<T> value<T>(StoredKey key) async {
    final prefs = await _prefs;
    final storedValue = prefs.get(key.rawKey);
    if (storedValue != null) {
      if (storedValue is List && storedValue.cast<String>() is T) {
        return storedValue.cast<String>() as T;
      }
      if (storedValue is T) {
        return storedValue as T;
      }
    }
    return _defaultValue<T>();
  }

  Future<void> apply<T>({
    required StoredKey key,
    required T value,
  }) async {
    final prefs = await _prefs;

    _castValue(
      value: value,
      onInt: (intVal) async {
        await prefs.setInt(key.rawKey, intVal);
      },
      onDouble: (doubleVal) async {
        await prefs.setDouble(key.rawKey, doubleVal);
      },
      onBool: (boolVal) async {
        await prefs.setBool(key.rawKey, boolVal);
      },
      onString: (stringVal) async {
        await prefs.setString(key.rawKey, stringVal);
      },
      onStringList: (stringListVal) async {
        await prefs.setStringList(key.rawKey, stringListVal);
      },
    );
  }

  T _defaultValue<T>() {
    if (isTypeOf<T, int>()) return 0 as T;
    if (isTypeOf<T, double>()) return 0.0 as T;
    if (isTypeOf<T, bool>()) return false as T;
    if (isTypeOf<T, String>()) return '' as T;
    if (isTypeOf<T, List<String>>()) return const <String>[] as T;
    if (isTypeOf<T, Map<String, double>>()) return const <String, double>{} as T;

    throw FallThroughError();
  }

  /*
   * supported types:
   * int double bool string stringList
   * */
  void _castValue<T>({
    required T value,
    required void Function(int intVal) onInt,
    required void Function(double doubleVal) onDouble,
    required void Function(bool boolVal) onBool,
    required void Function(String stringVal) onString,
    required void Function(List<String> stringListVal) onStringList,
  }) {
    if (isTypeOf<T, int>()) {
      onInt(value as int);
    } else if (isTypeOf<T, double>()) {
      onDouble(value as double);
    } else if (isTypeOf<T, bool>()) {
      onBool(value as bool);
    } else if (isTypeOf<T, String>()) {
      onString(value as String);
    } else if (isTypeOf<T, List<String>>()) {
      onStringList(value as List<String>);
    } else {
      throw FallThroughError();
    }
  }
}
