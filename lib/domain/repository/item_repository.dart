import 'package:cloud_firestore/cloud_firestore.dart';

import '../../model/api/api.dart';
import '../../model/api/response.dart';
import '../../model/extension/query_document_extension.dart';
import '../../model/item_model.dart';

const String _kManager = 'rooms';

class ItemRepository {

  ItemRepository({required Api api})
      : _api = api,
        _collection = FirebaseFirestore.instance.collection(_kManager);

  final Api _api;
  final CollectionReference _collection;

  Future<Response<List<ItemModel>>> getRooms() => _collection.get().then(_fromSnapshot);

  Response<List<ItemModel>> _fromSnapshot(QuerySnapshot querySnapshot) {
    return Response.succeed(
      querySnapshot.docs.map((doc) => ItemModel(
          id: doc.id,
          type: doc.mapData['type'] as String,
          name: doc.mapData['name'] as String,
        )).toList(),
    );
  }
}


