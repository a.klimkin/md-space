import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';

import '../../model/api/response.dart';
import '../../model/extension/query_document_extension.dart';
import '../../model/room_model.dart';

class RoomRepository {
  static const String manager = 'rooms';

  final CollectionReference _collection = FirebaseFirestore.instance.collection(manager);

  Future<Response<List<RoomModel>>> getRooms() => _collection.get().then(_fromSnapshot);

  Response<List<RoomModel>> _fromSnapshot(QuerySnapshot querySnapshot) {
    return Response.succeed(
      querySnapshot.docs.map((doc) => RoomModel(
          id: doc.id,
          roomName: doc.mapData['roomName'] as String,
          size: Size(
            doc.mapData['sizeWidth'] as double,
            doc.mapData['sizeHeight'] as double,
          ),
        )).toList(),
    );
  }
}
