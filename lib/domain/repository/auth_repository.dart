import 'package:firebase_auth/firebase_auth.dart';

import '../../model/api/api.dart';
import '../../model/api/response.dart';

class AuthRepository {
  final Api _api;
  final FirebaseAuth _auth;

  AuthRepository({required Api api}) :
        _api = api,
        _auth = FirebaseAuth.instance;

  Future<Response<UserCredential?>> createUserWithEmailAndPassword({
    required String email,
    required String password,
  }) => _api.executeAuth(
    execute: () => _auth.createUserWithEmailAndPassword(
      email: email,
      password: password,
    ),
  );

  Future<Response<UserCredential?>> signInWithEmailAndPassword({
    required String email,
    required String password,
  }) => _api.executeAuth(
    execute: () => _auth.signInWithEmailAndPassword(
      email: email,
      password: password,
    ),
  );

  Future<Response<void>> signOut() async => _api.executeAuth(
      execute: () async => _auth.signOut(),
    );
}