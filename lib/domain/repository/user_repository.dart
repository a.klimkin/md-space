import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../../model/api/api.dart';
import '../../model/api/response.dart';
import '../../model/user_model.dart';

const String _kManager = 'UserData';

class UserRepository {

  UserRepository({required Api api})
      : _api = api,
        _collection = FirebaseFirestore.instance.collection(_kManager);

  final Api _api;
  final CollectionReference _collection;


  User? getCurrentUser() => FirebaseAuth.instance.currentUser;

  Future<Response<UserModel?>> getCurrentUserData() =>
      _api.get(
        _collection,
        path: FirebaseAuth.instance.currentUser!.uid,
        deserialize: _fromSnapshot,
      );

  Future<Response<UserModel?>> getUserDataById(String uid) =>
      _api.get(
        _collection,
        path: uid,
        deserialize: _fromSnapshot,
      );

  Future<void> updateUserData(UserModel userData) =>
      _api.post(
        _collection,
        path: userData.uid,
        data: userData.toMap,
      );

  Future<UserModel> _fromSnapshot(Map<String, dynamic> doc) async {
    return UserModel(
        uid: doc['uid'] as String,
        firstName: doc['firstName'] as String,
        lastName: doc['lastName'] as String,
        email: doc['email'] as String,
        // role: (doc['role'] as String).userRoleValue,
        // department: doc['department'] as String,
        // photoURL: doc['photoURL'] as String,
        // position: doc['position'] as String,
        // fmcToken: doc['fmcToken'] as String,
    );
  }

}

extension _RowToString on String? {
  UserRole get userRoleValue {
    switch (this) {
      case 'user': 
        return UserRole.user;
      case 'admin':
        return UserRole.admin;
      default:
        return UserRole.undefined;
    }
  }
}
