import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:mdspace/model/booked_date.dart';

import '../../model/api/api.dart';
import '../../model/api/response.dart';
import '../../model/extension/query_document_extension.dart';
import '../../model/table_model.dart';

const String _kManager = 'tables';

class TableRepository {
  final Api _api;
  final CollectionReference _collection;

  TableRepository({required Api api})
      : _api = api,
        _collection = FirebaseFirestore.instance.collection(_kManager);

  Future<Response<List<TableModel>>> getTables() => _collection.get().then(_fromSnapshot);

  Future<Response<void>> bookTable({required String tableId, required DateTime bookedData} /* DateTimeRange range*/) async {
    return _collection.doc(tableId).collection('booked_date').doc(Timestamp.fromDate(bookedData).millisecondsSinceEpoch.toString())
        .set({'userId' : FirebaseAuth.instance.currentUser?.uid,}, SetOptions(merge: true))
        .then((value) => Response.succeed(value));
  }

  Future<Response<List<TableModel>>> _fromSnapshot(QuerySnapshot querySnapshot) async {
    return Response.succeed(
        querySnapshot.docs.map((doc) => TableModel(
          id: doc.id,
          roomId: doc.mapData['roomId'] as String,
          workerId: doc.mapData['workerId'] as String?,
          rotate: doc.mapData['rotate'] as int,
          size: Size(
            doc.mapData['sizeWidth'] as double,
            doc.mapData['sizeHeight'] as double,
          ),
          position: Offset(
            doc.mapData['positionDx'] as double,
            doc.mapData['positionDy'] as double,
          ),
          // type: doc.mapData['type'] as String?,
          // items: doc.mapData['items'] as List<String>,
        )).toList()
    );
  }
}
