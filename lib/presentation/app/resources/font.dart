import 'dart:ui';

import 'package:flutter/material.dart';

import 'palette.dart';

class Font extends TextStyle {
  const Font.w200({
    double size = 14,
    Color color = Palette.black,
    double height = 1.0
  }) : super(
    fontWeight: FontWeight.w200,
    fontFamily: 'Poppins',
    fontSize: size,
    color: color,
    height: height,
  );
  const Font.w300({
    double size = 14,
    Color color = Palette.black,
    double height = 1.0
  }) : super(
    fontWeight: FontWeight.w300,
    fontFamily: 'Poppins',
    fontSize: size,
    color: color,
    height: height,
  );
  const Font.w400({
    double size = 14,
    Color color = Palette.black,
    double height = 1.0
  }) : super(
    fontWeight: FontWeight.w400,
    fontFamily: 'Poppins',
    fontSize: size,
    color: color,
    height: height,
  );
  const Font.w500({
    double size = 14,
    Color color = Palette.black,
    double height = 1.0
  }) : super(
    fontWeight: FontWeight.w500,
    fontFamily: 'Poppins',
    fontSize: size,
    color: color,
    height: height,
  );
  const Font.w600({
    double size = 14,
    Color color = Palette.black,
    double height = 1.0
  }) : super(
    fontWeight: FontWeight.w600,
    fontFamily: 'Poppins',
    fontSize: size,
    color: color,
    height: height,
  );
  const Font.w700({
    double size = 14,
    Color color = Palette.black,
    double height = 1.0
  }) : super(
    fontWeight: FontWeight.w700,
    fontFamily: 'Poppins',
    fontSize: size,
    color: color,
    height: height,
  );
  const Font.w800({
    double size = 14,
    Color color = Palette.black,
    double height = 1.0
  }) : super(
    fontWeight: FontWeight.w800,
    fontFamily: 'Poppins',
    fontSize: size,
    color: color,
    height: height,
  );
  const Font.w900({
    double size = 14,
    Color color = Palette.black,
    double height = 1.0
  }) : super(
    fontWeight: FontWeight.w900,
    fontFamily: 'Poppins',
    fontSize: size,
    color: color,
    height: height,
  );
}
