import 'dart:ui';

class Palette {
  Palette._();

  static const transparent = Color(0x00000000);

  static const white = Color(0xFFFFFFFF);

  static const black = Color(0xFF000000);

  static const grey = Color(0xFFD0D0D0);
  static const grey_50 = Color(0x80D0D0D0);

  static const greyDarkest = Color(0xFF151616);
  static const greyDarkest_50 = Color(0x80151616);

  static const red = Color(0xFFF82D2D);

  static const green = Color(0xFF03DA73);

  static const blue = Color(0xFF0098EE);
  static const blue_50 = Color(0x800098EE);
}