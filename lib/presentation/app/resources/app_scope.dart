import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';


class ApplicationContext extends StatefulWidget {
  final Widget Function(GlobalKey<NavigatorState> navigatorKey) buildChild;

  const ApplicationContext({
    Key? key,
    required this.buildChild,
  }) : super(key: key);

  static ApplicationContextState? of(BuildContext context) => ApplicationScope.of(context)?.state;

  @override
  ApplicationContextState createState() => ApplicationContextState();
}

class ApplicationContextState extends State<ApplicationContext> {
  //AuthBloc authBloc;
  final GlobalKey<NavigatorState> _navigatorKey = GlobalKey();

  NavigatorState? get navigator => _navigatorKey.currentState;

  @override
  void initState() {
    super.initState();
    //authBloc = AuthBloc(repository: appDependencies.authRepository);
  }

  @override
  void dispose() {
    //authBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ApplicationScope(
      state: this,
      child: widget.buildChild(_navigatorKey),
    );
  }
}

@immutable
class ApplicationScope extends InheritedWidget {
  final ApplicationContextState state;

  const ApplicationScope({
    required Widget child,
    required this.state,
    Key? key,
  })  : assert(state is ApplicationContextState, 'ApplicationContextState must not be null'),
        super(key: key, child: child);

  static ApplicationScope? of(BuildContext context) =>
      context.getElementForInheritedWidgetOfExactType<ApplicationScope>()?.widget as ApplicationScope?;

  @override
  bool updateShouldNotify(ApplicationScope oldWidget) => state != oldWidget.state;
}
