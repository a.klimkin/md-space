class Constants {
  Constants._();

  static const double kAppBarHeight = 56.0;

  static const double k8 = 8.0;

  static const double k16 = 16.0;

  static const double k24 = 24.0;

  static const double kButtonHeight = 48.0;
  static const double kButtonWidth = 240.0;
}