import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:mdspace/presentation/ui/pages/auth/registration/screen.dart';

import '../../ui/pages/auth/sign_in/screen.dart';
import '../../ui/pages/auth/sign_in_strategy/screen.dart';
import '../../ui/pages/date_picker/screen.dart';
import '../../ui/pages/room/screen.dart';
import 'dependencies.dart';

class RouteFactory {
  Route<dynamic> getRoute(RouteSettings settings) {

    switch (settings.name) {

      case SignInScreen.route:
        return SignInScreen.createRoute(
          settings,
          authRepository: appDependencies.authRepository,
          userRepository: appDependencies.userRepository,
        );

      case SignUpScreen.route:
        return SignUpScreen.createRoute(
          settings,
          authRepository: appDependencies.authRepository,
          userRepository: appDependencies.userRepository,
        );

      case RoomScreen.route:
        return RoomScreen.createRoute(
          settings,
          repository: appDependencies.tableRepository,
        );

      case DatePickerScreen.route:
        return DatePickerScreen.createRoute(settings);

      case SignInStrategyScreen.route:
        return SignInStrategyScreen.createRoute(settings);

      default:
        throw FallThroughError();
    }
  }
}
