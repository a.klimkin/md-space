import 'package:get_it/get_it.dart';

import '../../../domain/repository/auth_repository.dart';
import '../../../domain/repository/storage/storage.dart';
import '../../../domain/repository/table_repository.dart';
import '../../../domain/repository/user_repository.dart';
import '../../../model/api/api.dart';
import 'route_factory.dart' as route_factory;

final GetIt _getIt = GetIt.instance;

Dependencies get appDependencies => _getIt.get<Dependencies>();

class Dependencies {
  final PreferencesStorage preferencesStorage;
  final route_factory.RouteFactory routes = route_factory.RouteFactory();
  final AuthRepository authRepository;
  final UserRepository userRepository;
  final TableRepository tableRepository;

  static Future<void> init() async {
    Dependencies._app();
  }

  factory Dependencies._app() {

    final preferencesStorage = PreferencesStorage();
    return Dependencies._build(
     preferencesStorage: preferencesStorage,
    );
  }

  factory Dependencies._build({
    required PreferencesStorage preferencesStorage,
  }) {
   final api = Api();

   return Dependencies._(
     preferencesStorage: preferencesStorage,
     authRepository: AuthRepository(api: api),
     userRepository: UserRepository(api: api),
     tableRepository: TableRepository(api: api),
   );
  }

  Dependencies._({
    required this.preferencesStorage,
    required this.authRepository,
    required this.userRepository,
    required this.tableRepository,
 }) {
    _getIt.registerSingleton<Dependencies>(this, signalsReady: true);
  }
}
