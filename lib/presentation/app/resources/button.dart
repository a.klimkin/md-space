import 'package:flutter/material.dart';

import 'palette.dart';

class Button {
  Button._();

  static ButtonStyle _baseStyle({
    Color? pressedColor = Palette.blue,
    Color disabledColor = Palette.grey,
    Color baseColor = Palette.green,
  }) =>
      ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states) {
          if (states.contains(MaterialState.disabled)) {
            return disabledColor;
          }
          return baseColor;
        }),
        overlayColor: MaterialStateProperty.resolveWith<Color?>((Set<MaterialState> states) {
          if (states.contains(MaterialState.pressed)) {
            return pressedColor;
          }
          return null;
        }),
        shape: MaterialStateProperty.all<OutlinedBorder>(
          const RoundedRectangleBorder(
            borderRadius:  BorderRadius.all(Radius.circular(24)),
          ),
        ),
      );

  static ButtonStyle green() => _baseStyle();

}
