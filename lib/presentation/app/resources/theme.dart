import 'package:flutter/material.dart';
import 'palette.dart';

class ThemeApp {

  ThemeData get appThemeData => ThemeData(
    brightness: Brightness.light,
    accentColor: Palette.green,
    primaryColor: Palette.blue,
    scaffoldBackgroundColor: Palette.white,
    backgroundColor: Palette.white,
    disabledColor: Palette.grey,
    dividerColor: Palette.greyDarkest,
    errorColor: Palette.red,
    hintColor: Palette.greyDarkest_50,
    splashColor: Palette.blue_50,
    highlightColor: Palette.blue_50,
    fontFamily: 'Poppins',
    appBarTheme: appBarTheme,
    visualDensity: VisualDensity.adaptivePlatformDensity,
  );

  AppBarTheme get appBarTheme => const AppBarTheme(
    color: Palette.white,
    centerTitle: true,
    elevation: 0.0,
  );


}
