import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:mdspace/presentation/ui/pages/splash/screen.dart';

import '../ui/pages/auth/sign_in/screen.dart';
import 'resources/app_scope.dart';
import 'resources/dependencies.dart';
import 'resources/theme.dart';

final RouteObserver<PageRoute<dynamic>> globalRouteObserver = RouteObserver<PageRoute<dynamic>>();

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData _themeData = ThemeApp().appThemeData;

    return ApplicationContext(
      buildChild: (navigatorKey) => MaterialApp(
        navigatorKey: navigatorKey,
        navigatorObservers: [globalRouteObserver],
        title: 'MD Space',
        theme: _themeData,
        themeMode: ThemeMode.light,
        home: const SplashScreen(),
        onGenerateRoute: appDependencies.routes.getRoute,
      ),
    );
  }
}
