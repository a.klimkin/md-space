// import 'package:flutter/material.dart';
// import 'package:mdspace/presentation/app/resources/constants.dart';
//
// Future<DateTimeRange?> showBookingRequest({
//   required BuildContext context,
// }) async {
//
//   return showDialog<DateTimeRange?>(
//     context: context,
//     barrierDismissible: false,
//     builder: (BuildContext context) {
//
//       return AlertDialog(
//         contentPadding: const EdgeInsets.all(Constants.k24),
//         shape: RoundedRectangleBorder(
//           borderRadius: BorderRadius.circular(Constants.k24),
//         ),
//         content: const SingleChildScrollView(
//           child: _BuildSelectDateBody(),
//         ),
//       );
//     },
//   );
// }
//
// class _BuildSelectDateBody extends StatefulWidget {
//   const _BuildSelectDateBody({
//     Key? key,
//   }) : super(key: key);
//
//   @override
//   __BuildSelectDateBodyState createState() => __BuildSelectDateBodyState();
// }
//
// class __BuildSelectDateBodyState extends State<_BuildSelectDateBody> {
//
//   Widget _space = const SizedBox(height: kMainSmallPadding);
//   DateTime _selectedDate = DateTime.now();
//
//   TextEditingController _startController = TextEditingController();
//   TextEditingController _endController = TextEditingController();
//
//   @override
//   Widget build(BuildContext context) {
//     return ListBody(
//       children: [
//         Text(
//           "Select the date for which you want to book the table.",
//           style: Theme.of(context).textTheme.subtitle1,
//           textAlign: TextAlign.center,
//         ),
//         Container(
//           width: 280,
//           padding: EdgeInsets.symmetric(horizontal: kMainPadding),
//           child: CalendarDatePicker(
//             firstDate: DateTime(2000),
//             lastDate: DateTime(2050),
//             initialDate: _selectedDate,
//             currentDate: DateTime.now(),
//             onDateChanged: (DateTime value) {
//               setState(() {
//                 _selectedDate = value;
//               });
//             },
//           ),
//         ),
//         Stack(
//           children: [
//             Padding(
//               padding: EdgeInsets.only(right: MediaQuery.of(context).size.width / 2.75),
//               child: Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: [
//                   Padding(
//                     padding: const EdgeInsets.only(left: kMainSmallPadding),
//                     child: Text("Start Time:",
//                       style: Theme.of(context).textTheme.subtitle1,
//                     ),
//                   ),
//                   CustomTextField(
//                     label: "00:00",
//                     controller: _startController,
//                   ),
//                 ],
//               ),
//             ),
//             Padding(
//               padding: EdgeInsets.only(left: MediaQuery.of(context).size.width / 2.75),
//               child: Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: [
//                   Padding(
//                     padding: const EdgeInsets.only(left: kMainSmallPadding),
//                     child: Text(
//                       "End Time:",
//                       style: Theme.of(context).textTheme.subtitle1,
//                     ),
//                   ),
//                   CustomTextField(
//                     label: "00:00",
//                     controller: _endController,
//                   ),
//                 ],
//               ),
//             ),
//           ],
//         ),
//         _space,
//         Padding(
//           padding: const EdgeInsets.symmetric(horizontal: kMainPadding),
//           child: PrimaryButton(
//             label: "Book",
//             onPressed: () {
//
//               DateTimeRange range = DateTimeRange(
//                 start: DateTime(
//                   _selectedDate.year,
//                   _selectedDate.month,
//                   _selectedDate.day,
//                   _getTimeBorder(_startController.text).hour,
//                   _getTimeBorder(_startController.text).minute,
//                 ),
//                 end: DateTime(
//                   _selectedDate.year,
//                   _selectedDate.month,
//                   _selectedDate.day,
//                   _getTimeBorder(_endController.text).hour,
//                   _getTimeBorder(_endController.text).minute,
//                 ),
//               );
//
//               Navigator.pop(context, range);
//             },
//           ),
//         ),
//         _space,
//         Padding(
//           padding: const EdgeInsets.symmetric(horizontal: kMainPadding),
//           child: WhiteButton(
//             label: "Close",
//             onPressed: () {
//               Navigator.pop(context, null);
//             },
//           ),
//         ),
//       ],
//     );
//   }
//
//   DateTime _getTimeBorder(String value) {
//     List<String> time = value.split(":");
//     return DateTime(1)
//       .add(Duration(hours: int.parse(time[0])))
//       .add(Duration(minutes: int.parse(time[1])));
//   }
// }
