// import 'package:flutter/material.dart';
// import 'package:table_booking/presentation/app/resources/resources.dart';
// import 'package:table_booking/presentation/ui/widgets/buttons/primary_button.dart';
// import 'package:table_booking/presentation/ui/widgets/buttons/white_button.dart';
// import 'package:table_booking/presentation/ui/widgets/text_field/custom_text_field.dart';
//
//
// // Create Request to Sys.Admin.
// Future<void> showItemRequest(
//     {BuildContext context,
//       @required VoidCallback onAccept,
//       @required TextEditingController controller,
//       VoidCallback onDecline}) async {
//   return _buildDialogWindow(
//     context: context,
//     title: "Please write down what you will need in your workplace.",
//     bodyList: [
//       Container(
//         child: CustomTextField(
//           label: "Text",
//           maxLines: 7,
//           controller: controller,
//         ),
//       ),
//     ],
//     onAccept: onAccept,
//     acceptButtonLabel: 'Send',
//     onDecline: onDecline,
//     declineButtonLabel: 'Close',
//   );
// }
//
// // Builder
// Future<void> _buildDialogWindow({
//   @required BuildContext context,
//   @required String title,
//   List<Widget> bodyList,
//   @required VoidCallback onAccept,
//   @required String acceptButtonLabel,
//   VoidCallback onDecline,
//   String declineButtonLabel,
// }) async {
//   var _listBody = <Widget>[];
//
//   Widget _space = const SizedBox(height: kMainSmallPadding);
//
//   return showDialog<void>(
//     context: context,
//     barrierDismissible: false,
//     builder: (BuildContext context) {
//
//       _listBody.add(Text(
//         title,
//         style: Theme.of(context).textTheme.subtitle1,
//         textAlign: TextAlign.center,
//       ));
//
//       _listBody.add(_space);
//
//       if(bodyList != null) {
//         _listBody.addAll(bodyList);
//         _listBody.add(_space);
//       }
//
//       _listBody.add(
//         Padding(
//           padding: const EdgeInsets.symmetric(horizontal: kMainPadding),
//           child: PrimaryButton(
//             label: acceptButtonLabel,
//             onPressed: () {
//               Navigator.pop(context);
//               onAccept();
//             },
//           ),
//         ),
//       );
//
//       if (onDecline != null) {
//         _listBody.add(_space);
//
//         _listBody.add(Padding(
//           padding: const EdgeInsets.symmetric(horizontal: kMainPadding),
//           child: WhiteButton(
//             label: declineButtonLabel,
//             onPressed: () {
//               Navigator.pop(context);
//               onDecline();
//             },
//           ),
//         ));
//       }
//
//       return AlertDialog(
//         contentPadding: EdgeInsets.all(kMainPadding),
//         shape: RoundedRectangleBorder(
//           borderRadius: BorderRadius.circular(kMainPadding),
//         ),
//         content: SingleChildScrollView(
//           child: ListBody(
//             children: _listBody,
//           ),
//         ),
//       );
//     },
//   );
// }