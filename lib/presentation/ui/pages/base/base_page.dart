// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:table_booking/presentation/ui/pages/base/bloc/base_bloc.dart';
// import 'package:table_booking/presentation/ui/utils/title_util.dart';
// import 'package:table_booking/presentation/ui/widgets/drawer/drawer.dart';
//
//
// class BasePage extends StatefulWidget {
//   final Widget body;
//
//   const BasePage({Key key, this.body}) : super(key: key);
//
//
//   @override
//   _BasePageState createState() => _BasePageState();
// }
//
// class _BasePageState extends State<BasePage> {
//
//   @override
//   Widget build(BuildContext context) {
//
//     return BlocProvider<BaseBloc>(
//       create: (context) => BaseBloc()..add(InitProfile(context)),
//       child: BlocBuilder<BaseBloc, BaseState>(
//           builder: (context, state) => CustomDrawer(
//             showDrawer: state is ShowDrawer,
//             child: Scaffold(
//               appBar: AppBar(
//                 title: Text(
//                   TitleUtil.identifyTitle(widget.body),
//                   style: Theme.of(context).textTheme.headline6,
//                 ),
//                 actions: [
//                   IconButton(
//                     icon: Icon(
//                       Icons.person_outline_sharp,
//                     ),
//                     onPressed: () {
//                       BlocProvider.of<BaseBloc>(context).add(
//                         PressedShowDrawer(context),
//                       );
//                     },
//                   ),
//                 ],
//               ),
//               body: widget.body,
//             ),
//           ),
//         ),
//
//     );
//   }
// }
