// import 'dart:async';
//
// import 'package:bloc/bloc.dart';
// import 'package:equatable/equatable.dart';
// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:flutter/material.dart';
// import 'package:table_booking/domain/use_cases/use_cases.dart';
// import 'package:table_booking/presentation/ui/pages/auth/registration/registration_page.dart';
// import 'package:table_booking/presentation/ui/pages/auth/sign_in/sign_in_page.dart';
// import 'package:table_booking/presentation/ui/pages/main/main_page.dart';
//
// part 'base_event.dart';
// part 'base_state.dart';
//
// class BaseBloc extends Bloc<BaseEvent, BaseState> {
//   BaseBloc() : super(BaseInitial());
//
//   final UseCases _useCases = UseCases();
//
//   User _user;
//
//   @override
//   Stream<BaseState> mapEventToState(
//     BaseEvent event,
//   ) async* {
//     print(event);
//     if (event is InitProfile) {
//       yield InputUserData(_useCases.userManager.getCurrentUser());
//     } else if (event is PressedShowDrawer) {
//       yield ShowDrawer();
//     } else if (event is DrawerHided) {
//       yield HideDrawer();
//     } else if (event is SignInPressed) {
//       Navigator.of(event.context).pushNamed(SignInPage.routeName);
//     } else if (event is RegistrationPressed) {
//       Navigator.of(event.context).pushNamed(RegistrationPage.routeName);
//     } else if (event is LogOutPressed) {
//       _useCases.authManager.signOut();
//
//       Navigator.of(event.context).pushNamedAndRemoveUntil(MainPage.routeName, (route) => false);
//     }
//   }
// }
