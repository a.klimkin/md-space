// part of 'base_bloc.dart';
//
// abstract class BaseEvent extends Equatable {
//   final BuildContext context;
//   const BaseEvent(this.context);
//
//   @override
//   List<Object> get props => [];
// }
//
// class InitProfile extends BaseEvent {
//   InitProfile(BuildContext context) : super(context);
// }
//
// class PressedShowDrawer extends BaseEvent {
//   PressedShowDrawer(BuildContext context) : super(context);
// }
//
// class DrawerHided extends BaseEvent {
//   DrawerHided(BuildContext context) : super(context);
// }
//
// class PressedPop extends BaseEvent {
//   PressedPop(BuildContext context) : super(context);
// }
//
// class SignInPressed extends BaseEvent {
//   SignInPressed(BuildContext context) : super(context);
// }
//
// class RegistrationPressed extends BaseEvent {
//   RegistrationPressed(BuildContext context) : super(context);
// }
//
// class LogOutPressed extends BaseEvent {
//   LogOutPressed(BuildContext context) : super(context);
// }
//
// class EditProfilePressed extends BaseEvent {
//   EditProfilePressed(BuildContext context) : super(context);
// }