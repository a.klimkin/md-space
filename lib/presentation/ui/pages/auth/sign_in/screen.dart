import 'package:flutter/material.dart';
import 'package:mdspace/model/room_model.dart';
import 'package:mdspace/presentation/ui/widgets/state_listener/widget.dart';
import 'package:provider/provider.dart';

import '../../../../../domain/bloc/sign_in_bloc.dart';
import '../../../../../domain/repository/auth_repository.dart';
import '../../../../../domain/repository/user_repository.dart';
import '../../../../app/resources/app_scope.dart';
import '../../../../app/resources/constants.dart';
import '../../../../app/resources/font.dart';
import '../../../../app/resources/palette.dart';
import '../../../widgets/buttons/custom_elevated_button.dart';
import '../../../widgets/custom_text_field/widget.dart';
import '../../room/screen.dart';


class SignInScreen extends StatefulWidget {
  static const route = '/SignInScreen';

  static PageRoute<dynamic> createRoute(
      RouteSettings settings, {
        required AuthRepository authRepository,
        required UserRepository userRepository,
      }) {
    return MaterialPageRoute<Provider<SignInBloc>>(
        settings: settings,
        builder: (BuildContext context) {
          return Provider<SignInBloc>(
            create: (_) => SignInBloc(
              userRepository: userRepository,
              authRepository: authRepository,
            ),
            dispose: (_, bloc) => bloc.dispose(),
            child: const SignInScreen(),
          );
        });
  }

  const SignInScreen({Key? key}) : super(key: key);

  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInScreen> {

  final FocusNode emailFocusNode = FocusNode();
  final FocusNode passwordFocusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    final SignInBloc bloc = Provider.of<SignInBloc>(context, listen: false);

    return Scaffold(
      body: StreamListener<SignInState>(
        stream: bloc.outStream,
        onValueReceived: (state) {
          if (state.status == SignInStatus.authorized) {
            Navigator.of(context).pushNamed(
                RoomScreen.route,
                arguments: RoomScreenArguments(roomModel: const RoomModel(
                  id: 'BnnC2URwE1wG6IOftf2Y',
                  roomName: 'WR 1-1',
                  size: Size(520, 1240),
                ))
            );
          }
        },
        child: StreamBuilder<SignInState>(
          initialData: bloc.currentState,
          stream: bloc.outStream,
          builder: (context, snapshot) {
            final state = snapshot.data!;

            return Column(
              children: [
                const SizedBox(height: 180),
                // const Placeholder(fallbackHeight: 100, fallbackWidth: 100),
                const Text(
                  "Sign In",
                  style: Font.w600(size: 18,height: 1.5),
                ),
                const SizedBox(height: Constants.k24),
                CustomInputField(
                  autocorrect: false,
                  focusNode: emailFocusNode,
                  autofocus: true,
                  textValue: state.email,
                  labelText: 'Email',
                  hintText: 'Enter your email',
                  keyboardType: TextInputType.emailAddress,
                  textCapitalization: TextCapitalization.none,
                  errorStream: bloc.validationErrorStream(fieldType: LoginFieldType.email),
                  onTextChanged: (text) {
                    bloc.updateFormData(
                      fieldType: LoginFieldType.email,
                      newValue: text,
                    );
                  },
                  onSubmitted: (_) => passwordFocusNode.requestFocus(),
                ),
                const SizedBox(height: Constants.k24),
                CustomInputField(
                  autocorrect: false,
                  focusNode: passwordFocusNode,
                  textValue: state.password,
                  labelText: 'Password',
                  hintText: 'Enter your Password',
                  keyboardType: TextInputType.visiblePassword,
                  obscureText: true,
                  textCapitalization: TextCapitalization.none,
                  errorStream: bloc.validationErrorStream(fieldType: LoginFieldType.password),
                  onTextChanged: (text) {
                    bloc.updateFormData(
                      fieldType: LoginFieldType.password,
                      newValue: text,
                    );
                  },
                  onSubmitted: (_) => FocusScope.of(context).unfocus(),
                ),
                const SizedBox(height: 48),
                CustomElevatedButton(
                  onPressed: () {
                    bloc.validateFields().then((isValid) {
                      if (isValid) {
                        bloc.signIn();
                      }
                    });
                  },
                  child: const Text(
                    'Sign in',
                    style: Font.w500(size: 16, color: Palette.white),
                  ),
                )
              ],
            );
          }
        ),
      ),
    );
  }
}
