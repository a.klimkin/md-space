// import 'dart:async';
//
// import 'package:bloc/bloc.dart';
// import 'package:equatable/equatable.dart';
// import 'package:flutter/material.dart';
// import 'package:table_booking/data/entity/entities.dart';
// import 'package:table_booking/domain/use_cases/use_cases.dart';
// import 'package:table_booking/presentation/ui/pages/main/main_page.dart';
//
// part 'registration_page_event.dart';
// part 'registration_page_state.dart';
//
// class RegistrationPageBloc extends Bloc<RegistrationPageEvent, RegistrationPageState> {
//   RegistrationPageBloc() : super(RegistrationPageInitial());
//
//   final UseCases _useCases = UseCases();
//
//   @override
//   Stream<RegistrationPageState> mapEventToState(
//     RegistrationPageEvent event,
//   ) async* {
//     if (event is ShowPassword) {
//       yield CleanErrors();
//       yield ObscurePassword(!event.obscureText);
//
//     } else if (event is ShowConfirmPassword) {
//       yield CleanErrors();
//       yield ObscureConfirmPassword(!event.obscureText);
//
//     } else if (event is RegistrationPressed) {
//       yield CleanErrors();
//       // loading
//
//       if (event.email.isEmpty) {
//         yield EmailError();
//       }
//       if (event.password.isEmpty) {
//         yield PasswordError();
//       }
//       if (event.confirmPassword.isEmpty && (event.password != event.confirmPassword)) {
//         yield ConfirmPasswordError();
//       }
//
//       if (event.email.isNotEmpty
//           && event.password.isNotEmpty
//           && (event.confirmPassword.isNotEmpty && (event.password == event.confirmPassword))) {
//         AuthStates authStates =  await _useCases
//             .authManager
//             .createUserWithEmailAndPassword(
//           email: event.email,
//           password: event.password,
//         );
//
//         switch (authStates) {
//           case AuthStates.success : {
//             Navigator.of(event.context).pushNamedAndRemoveUntil(MainPage.routeName, (route) => false);
//           }
//           break;
//           case AuthStates.emailExists : {
//             yield EmailError();
//           }
//           break;
//           case AuthStates.weakPassword : {
//             yield PasswordError();
//           }
//           break;
//           default: {
//             yield CleanErrors();
//           }
//         }
//       }
//     }
//   }
// }
