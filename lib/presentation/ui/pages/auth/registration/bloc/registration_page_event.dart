// part of 'registration_page_bloc.dart';
//
// abstract class RegistrationPageEvent extends Equatable {
//   final BuildContext context;
//   const RegistrationPageEvent(this.context);
//
//   @override
//   List<Object> get props => [];
// }
//
// class RegistrationPressed extends RegistrationPageEvent {
//   final String email;
//   final String password;
//   final String confirmPassword;
//
//   RegistrationPressed({
//     BuildContext context,
//     this.email,
//     this.password,
//     this.confirmPassword,
//   }) : super(context);
//
//   @override
//   List<Object> get props => [email, password, confirmPassword];
// }
//
// class ShowPassword extends RegistrationPageEvent {
//   final bool obscureText;
//   ShowPassword({BuildContext context, this.obscureText}) : super(context);
//
//   @override
//   List<Object> get props => [obscureText];
// }
//
// class ShowConfirmPassword extends RegistrationPageEvent {
//   final bool obscureText;
//   ShowConfirmPassword({BuildContext context, this.obscureText}) : super(context);
//
//   @override
//   List<Object> get props => [obscureText];
// }
