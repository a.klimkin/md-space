// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:table_booking/presentation/app/resources/resources.dart';
// import 'package:table_booking/presentation/ui/pages/auth/registration/bloc/registration_page_bloc.dart';
// import 'package:table_booking/presentation/ui/widgets/buttons/primary_button.dart';
// import 'package:table_booking/presentation/ui/widgets/text_field/custom_text_field.dart';
//
// class RegistrationPage extends StatefulWidget {
//   static final routeName = '/registration';
//
//   @override
//   _RegistrationPageState createState() => _RegistrationPageState();
// }
//
// class _RegistrationPageState extends State<RegistrationPage> {
//   final _formKey = GlobalKey<FormState>();
//
//   final TextEditingController _emailController = TextEditingController();
//   final TextEditingController _passwordController = TextEditingController();
//   final TextEditingController _confirmPasswordController =
//       TextEditingController();
//
//   @override
//   Widget build(BuildContext context) {
//     var errorMessage = "";
//
//     var emailError = false;
//     var passwordError = false;
//     var confirmPasswordError = false;
//
//     var obscurePassword = true;
//     var obscureConfirmPassword = true;
//
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Theme.of(context).scaffoldBackgroundColor,
//       ),
//       body: BlocProvider<RegistrationPageBloc>(
//         create: (context) => RegistrationPageBloc(),
//         child: BlocListener<RegistrationPageBloc, RegistrationPageState>(
//           listener: (context, state) {
//             if (state is CleanErrors) {
//               errorMessage = "";
//               emailError = false;
//               passwordError = false;
//               confirmPasswordError = false;
//             } else if (state is EmailError) {
//               errorMessage = "Email not found";
//               emailError = true;
//             } else if (state is PasswordError) {
//               errorMessage = "Wrong password";
//               passwordError = true;
//             } else if (state is ConfirmPasswordError) {
//               errorMessage = "Passwords do not match";
//               confirmPasswordError = true;
//             } else if (state is ObscurePassword) {
//               obscurePassword = state.obscurePassword;
//             } else if (state is ObscureConfirmPassword) {
//               obscureConfirmPassword = state.obscureConfirmPassword;
//             }
//           },
//           child: BlocBuilder<RegistrationPageBloc, RegistrationPageState>(
//             builder: (context, state) => SizedBox(
//               height: MediaQuery.of(context).size.height,
//               child: Padding(
//                 padding: const EdgeInsets.all(kMainPadding),
//                 child: Form(
//                   key: _formKey,
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.center,
//                     children: [
//                       Spacer(flex: 2),
//                       Placeholder(fallbackHeight: 100, fallbackWidth: 100),
//                       Spacer(flex: 2),
//                       Text(
//                         "Sign Up",
//                         style: Theme.of(context).textTheme.headline6,
//                       ),
//                       Container(
//                         padding: EdgeInsets.all(kMainSmallPadding),
//                         child: Text(
//                           errorMessage,
//                           style: Theme.of(context).textTheme.subtitle1.copyWith(
//                             color: Theme.of(context).errorColor,
//                           ),
//                         ),
//                       ),
//                       CustomTextField(
//                         label: "Email",
//                         controller: _emailController,
//                         hasError: emailError,
//                       ),
//                       CustomTextField(
//                         label: "Password",
//                         controller: _passwordController,
//                         obscureText: obscurePassword,
//                         hasError: passwordError,
//                         suffixIcon: IconButton(
//                           icon: Icon(Icons.remove_red_eye),
//                           onPressed: () {
//                             BlocProvider.of<RegistrationPageBloc>(context).add(
//                               ShowPassword(
//                                 context: context,
//                                 obscureText: obscurePassword,
//                               ),
//                             );
//                           },
//                         ),
//                       ),
//                       CustomTextField(
//                         label: "Confirm Password",
//                         controller: _confirmPasswordController,
//                         obscureText: obscureConfirmPassword,
//                         hasError: confirmPasswordError,
//                         suffixIcon: IconButton(
//                           icon: Icon(Icons.remove_red_eye),
//                           onPressed: () {
//                             BlocProvider.of<RegistrationPageBloc>(context).add(
//                               ShowConfirmPassword(
//                                 context: context,
//                                 obscureText: obscureConfirmPassword,),
//                             );
//                           },
//                         ),
//                       ),
//                       Spacer(flex: 2),
//                       PrimaryButton(
//                         label: "Registration",
//                         onPressed: () {
//                           BlocProvider.of<RegistrationPageBloc>(context)
//                               .add(
//                             RegistrationPressed(
//                               context: context,
//                               email: _emailController.text,
//                               password: _passwordController.text,
//                               confirmPassword: _confirmPasswordController.text,
//                             ),
//                           );
//                         },
//                       ),
//                       Spacer(flex: 2),
//                     ],
//                   ),
//                 ),
//               ),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }
