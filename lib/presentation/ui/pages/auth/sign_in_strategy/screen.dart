import 'package:flutter/material.dart';
import 'package:mdspace/presentation/app/resources/app_scope.dart';
import 'package:mdspace/presentation/app/resources/constants.dart';
import 'package:mdspace/presentation/app/resources/font.dart';
import 'package:mdspace/presentation/app/resources/palette.dart';
import 'package:mdspace/presentation/ui/pages/auth/registration/screen.dart';
import 'package:mdspace/presentation/ui/pages/auth/sign_in/screen.dart';
import 'package:mdspace/presentation/ui/widgets/buttons/custom_elevated_button.dart';

class SignInStrategyScreen extends StatefulWidget {
  static const route = '/SignInStrategyScreen';

  static PageRoute<dynamic> createRoute(RouteSettings settings) {
    return MaterialPageRoute(
        settings: settings,
        builder: (BuildContext context) => const SignInStrategyScreen());
  }

  const SignInStrategyScreen({Key? key}) : super(key: key);

  @override
  _SignInStrategyScreenState createState() => _SignInStrategyScreenState();
}

class _SignInStrategyScreenState extends State<SignInStrategyScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            const SizedBox(height: 180),
            const Text(
              "MD Space",
              style: Font.w600(size: 18,height: 1.5),
            ),
            const SizedBox(height: Constants.k24),
            CustomElevatedButton(
              onPressed: () {
                ApplicationContext.of(context)?.navigator?.pushReplacementNamed(SignUpScreen.route);
              },
              child: const Text(
                'Sign up',
                style: Font.w500(size: 16, color: Palette.white),
              ),
            ),
            const SizedBox(height: Constants.k24),
            CustomElevatedButton(
              onPressed: () {
                ApplicationContext.of(context)?.navigator?.pushReplacementNamed(SignInScreen.route);
              },
              child: const Text(
                'Sign in',
                style: Font.w500(size: 16, color: Palette.white),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
