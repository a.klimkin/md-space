// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:table_booking/data/entity/entities.dart';
// import 'package:table_booking/domain/use_cases/use_cases.dart';
// import 'package:table_booking/presentation/app/resources/resources.dart';
// import 'package:table_booking/presentation/ui/pages/base/base_page.dart';
// import 'package:table_booking/presentation/ui/pages/main/bloc/main_page_bloc.dart';
// import 'package:table_booking/presentation/ui/utils/floors_util.dart';
// import 'package:table_booking/presentation/ui/widgets/buttons/primary_button.dart';
// import 'package:table_booking/presentation/ui/widgets/buttons/round_floor_button.dart';
// import 'package:table_booking/presentation/ui/widgets/items/transformed_flore_view.dart';
//
// class MainPage extends StatelessWidget {
//   static final routeName = '/main';
//
//   @override
//   Widget build(BuildContext context) {
//     return BasePage(
//       body: MainPageBody(),
//     );
//   }
// }
//
// class MainPageBody extends StatelessWidget {
//   const MainPageBody({
//     Key key,
//   }) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     Floors selectedFloorTag;
//
//     return BlocProvider<MainPageBloc>(
//       create: (context) => MainPageBloc(),
//       child: BlocListener<MainPageBloc, MainPageState>(
//         listener: (context, state) {
//           if (state is SelectFloor) {
//             selectedFloorTag = state.floorTag;
//           }
//         },
//         child: BlocBuilder<MainPageBloc, MainPageState>(
//           builder: (context, state) => SizedBox(
//             height: MediaQuery.of(context).size.height - kToolbarHeight,
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 Spacer(
//                   flex: 1,
//                 ),
//                 Center(
//                   child: Text(
//                     FloorUtil.getTitleByFloor(selectedFloorTag),
//                     style: Theme.of(context).textTheme.headline6,
//                   ),
//                 ),
//                 Spacer(
//                   flex: 1,
//                 ),
//                 Stack(
//                     children: Floors.values.map((floorTag) => FloorTransformView(
//                       isFocused: floorTag == selectedFloorTag,
//                       spaceFromTop: (200.0 - FloorUtil.getFloorNumber(floorTag) * 100.0),
//                     )).toList()
//                 ),
//                 Transform.translate(
//                   offset: Offset(24, -200),
//                   child: Column(
//                       verticalDirection: VerticalDirection.up,
//                       children: Floors.values.map((floorTag) => Column(
//                         children: [
//                           Padding(
//                             padding: const EdgeInsets.only(bottom: 52.0),
//                             child: RoundFloorButton(
//                               isFocused: floorTag == selectedFloorTag,
//                               label: FloorUtil.getFloorNumber(floorTag).toString(),
//                               onPressed: () {
//                                 BlocProvider.of<MainPageBloc>(context).add(
//                                   FloorSelected(
//                                     context: context,
//                                     floorTag: floorTag,
//                                   ),
//                                 );
//                               },
//                             ),
//                           ),
//                         ],
//                       )).toList()
//                   ),
//                 ),
//                 Center(
//                   child: selectedFloorTag != null ? PrimaryButton(
//                     label: "Show the plan",
//                     onPressed: () {
//                       BlocProvider.of<MainPageBloc>(context).add(
//                         WentToSelectedFloor(context),
//                       );
//                     },
//                   ) : SizedBox(height: kButtonHeight),
//                 ),
//                 Spacer(
//                   flex: 2,
//                 ),
//               ],
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }
