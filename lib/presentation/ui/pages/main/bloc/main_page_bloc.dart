// import 'dart:async';
//
// import 'package:bloc/bloc.dart';
// import 'package:equatable/equatable.dart';
// import 'package:flutter/material.dart';
// import 'package:table_booking/data/entity/entities.dart';
// import 'package:table_booking/presentation/ui/pages/room/room_page.dart';
//
// part 'main_page_event.dart';
// part 'main_page_state.dart';
//
// class MainPageBloc extends Bloc<MainPageEvent, MainPageState> {
//   MainPageBloc() : super(MainPageInitial());
//
//   @override
//   Stream<MainPageState> mapEventToState(
//     MainPageEvent event,
//   ) async* {
//     if (event is FloorSelected) {
//       yield SelectFloor(event.floorTag);
//     } else if (event is WentToSelectedFloor) {
//       Navigator.of(event.context).pushNamed(RoomPage.routeName);
//     } else if (event is PressedShowDrawer) {
//       yield ShowDrawer();
//     } else if (event is HidedDrawer) {
//       yield HideDrawer();
//     }
//   }
// }
