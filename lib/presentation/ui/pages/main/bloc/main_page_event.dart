// part of 'main_page_bloc.dart';
//
// abstract class MainPageEvent extends Equatable {
//   final BuildContext context;
//   const MainPageEvent(this.context);
//
//   @override
//   List<Object> get props => [];
// }
//
// class FloorSelected extends MainPageEvent {
//   final Floors floorTag;
//
//   FloorSelected({BuildContext context, this.floorTag}) : super(context);
//
//   @override
//   List<Object> get props => [floorTag];
// }
//
// class WentToSelectedFloor extends MainPageEvent {
//   WentToSelectedFloor(BuildContext context) : super(context);
// }
//
// class PressedShowDrawer extends MainPageEvent {
//   PressedShowDrawer(BuildContext context) : super(context);
// }
//
// class HidedDrawer extends MainPageEvent {
//   HidedDrawer(BuildContext context) : super(context);
// }
//
