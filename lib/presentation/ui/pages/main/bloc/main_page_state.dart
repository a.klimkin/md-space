// part of 'main_page_bloc.dart';
//
// abstract class MainPageState extends Equatable {
//   const MainPageState();
//
//   @override
//   List<Object> get props => [];
// }
//
// class MainPageInitial extends MainPageState {}
//
// class SelectFloor extends MainPageState {
//   final Floors floorTag;
//
//   SelectFloor(this.floorTag);
//
//   @override
//   List<Object> get props => [floorTag];
// }
//
// class ShowDrawer extends MainPageState {}
//
// class HideDrawer extends MainPageState {}