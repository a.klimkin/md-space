import 'package:flutter/material.dart';

import 'day.dart';
import 'daystub.dart';

class WeekWidget extends StatelessWidget {
  final DateTime startDate;
  final DateTime endDate;
  final DateTime currentDate;
  final DateTime? selectedDate;
  final int number;
  final Function(DateTime date) onDayClicked;
  final bool firstRow;
  final bool lastRow;

  WeekWidget({
    required this.startDate,
    required this.endDate,
    required this.currentDate,
    required this.selectedDate,
    required this.firstRow,
    required this.lastRow,
    required this.onDayClicked,
  }) : number = getWeekForDay(startDate.year, startDate.month, startDate.day);

  void Function() _buildOnDayTap(BuildContext context, DateTime date) {
    return () {
      onDayClicked(date);
    };
  }

  List<Widget> _buildLayout(BuildContext context) {
    int firstDayOfTheWeek;
    int lastDayOfTheWeek;

    firstDayOfTheWeek = DateTime.monday;
    lastDayOfTheWeek = DateTime.sunday;

    final daysDiff = endDate.day - startDate.day + 1;
    Set<int> visibleWeekDays;
    if (daysDiff == 7) {
      visibleWeekDays = const {
        DateTime.sunday,
        DateTime.monday,
        DateTime.tuesday,
        DateTime.wednesday,
        DateTime.thursday,
        DateTime.friday,
        DateTime.saturday,
      };
    } else {
      visibleWeekDays = List.generate(daysDiff, (index) {
        final candidate = endDate.weekday - index;
        return candidate == 0 ? DateTime.sunday : candidate;
      }).toSet();
    }
    final List<int> weekDays = List<int>.from(const [
      DateTime.monday,
      DateTime.tuesday,
      DateTime.wednesday,
      DateTime.thursday,
      DateTime.friday,
      DateTime.saturday,
      DateTime.sunday,
    ]).toList();

    return weekDays.map((weekDayIndex) {
      if (!visibleWeekDays.contains(weekDayIndex)) {
        return DayStubWidget();
      } else {
        final DateTime date = DateTime(
          startDate.year,
          startDate.month,
          startDate.day + (weekDayIndex - startDate.weekday),
        );

        final lastDayDateTime =
        (date.month < 12) ? DateTime(date.year, date.month + 1, 0) : DateTime(date.year + 1, 1, 0);

        return DayWidget(
          firstWeekDay: weekDayIndex == firstDayOfTheWeek ||
              startDate.day + (weekDayIndex - startDate.weekday) == firstDayOfTheWeek,
          lastWeekDay: weekDayIndex == lastDayOfTheWeek || date == lastDayDateTime,
          date: date,
          isCurrent: date == currentDate,
          isSelected: date == selectedDate,
          onTap: _buildOnDayTap(context, date),
          index: weekDayIndex,
          isPastDay: date.isBefore(currentDate),
        );
      }
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 60.0,
      child: Row(
        children: _buildLayout(context),
      ),
    );
  }
}

int getWeekForDay(int year, int month, int day) {
  final DateTime now = DateTime(year, month, day);

  // method states that week 1 is the week
  // with the first monday of that year.
  // Set the target date to the monday in the target week
  int dayNr = (now.weekday + 6) % 7;
  final DateTime thisWeekMonday = now.subtract(Duration(days: dayNr));

  // Get first monday at start of the year
  final DateTime startOfYearMonday = DateTime(year, DateTime.january);
  dayNr = (startOfYearMonday.weekday + 6) % 7;
  startOfYearMonday.add(Duration(days: dayNr));

  // The weeknumber is the number of weeks between the
  // first thursday of the year and the thursday in the target week
  final int x = thisWeekMonday.difference(startOfYearMonday).inDays;
  // .round() is a dirty hack for correct calculation week,
  // with started from Sunday.
  // it possible to remove it if we start our calculation from day,
  // with depends from locale(Sunday or Monday)
  final int weekNumber = (x / 7).round() + 1;

  return weekNumber;
}

int getAmountOfWeeks(DateTime start, DateTime end) {
  final int amountOfDaysToCurrentWeek = 7 - ((6 + start.weekday) % 7);
  final int amountOfDaysFromDate = end.difference(start).inDays;
  final amountOfWeeksWithoutCurrentWeek =
      (amountOfDaysFromDate - amountOfDaysToCurrentWeek) / 7;
  final amountOfWeeks = amountOfWeeksWithoutCurrentWeek +
      (amountOfWeeksWithoutCurrentWeek is double ? 2 : 1);

  return amountOfWeeks.toInt();
}
