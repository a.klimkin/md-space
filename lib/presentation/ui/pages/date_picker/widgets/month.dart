import 'package:flutter/material.dart';

import 'month_header.dart';
import 'week.dart';

class MonthWidget extends StatelessWidget {
  final DateTime date;
  final DateTime current;
  final Function(DateTime date) onDayClicked;
  final DateTime? selected;

  const MonthWidget({
    required this.date,
    required this.onDayClicked,
    required this.current,
    this.selected,
  });

  @override
  Widget build(BuildContext context) {
    final List<Widget> weeks = [];

    weeks.add(MonthHeaderWidget(date: date));

    final DateTime previousMonthStartOfTheWeek = date.subtract(Duration(days: date.weekday - 1));

    final int firstDayOfTheWeek = DateTime.monday;

    final bool previousMonthIsFilled = date.weekday == firstDayOfTheWeek;

    int index = 0;
    do {
      DateTime start;
      if (index == 0) {
        start = date;
      } else {
        if (!previousMonthIsFilled) {
          start = previousMonthStartOfTheWeek;
        } else {
          start = date;
        }
      }
      start = start.add(Duration(days: index * 7, hours: 2));
      start = DateTime(start.year, start.month, start.day); // fix summertime switching

      DateTime end = start.add(const Duration(days: 6, hours: 2));
      end = DateTime(end.year, end.month, end.day); // fix summertime switching

      if (start.day == 1 && start.weekday > DateTime.monday) {
        end = start.add(Duration(days: 7 - start.weekday));
      }

      if (end.month != start.month) {
        // previous day of next month will be last date of month
        end = DateTime(start.month == 12 ? start.year + 1 : start.year, start.month == 12 ? 1 : start.month + 1, 0);
      }
      final lastRow = end.nextDayStart.month != date.month;
      weeks.add(WeekWidget(
        firstRow: index == 0,
        lastRow: lastRow,
        startDate: start,
        endDate: end,
        currentDate: current,
        selectedDate: selected,
        onDayClicked: onDayClicked,
      ));

      index += 1;

      // break if next start of week will be in next month
      if (lastRow) {
        break;
      }
    } while (true);

    return Column(
      children: weeks,
    );
  }
}

extension DateTimeExt on DateTime {
  DateTime get nextDayStart {
    if (isUtc) {
      return DateTime.utc(year, month, day, 24);
    } else {
      return DateTime(year, month, day, 24);
    }
  }

  DateTime get emitHHmmSs {
    if (isUtc) {
      return DateTime.utc(year, month, day);
    } else {
      return DateTime(year, month, day);
    }
  }
}
