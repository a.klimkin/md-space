import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mdspace/presentation/app/resources/font.dart';

const double kMonthHeaderHeight = 54.0;

class MonthHeaderWidget extends StatelessWidget {
  final DateTime date;

  const MonthHeaderWidget({
    required this.date,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: kMonthHeaderHeight,
      child: Padding(
        padding: const EdgeInsets.only(left: 10),
        child: Align(
          alignment: Alignment.bottomLeft,
          child: Text(
            DateFormat('MMMM yyyy').format(date),
            style: const Font.w600(size: 23),
          ),
        ),
      ),
    );
  }
}
