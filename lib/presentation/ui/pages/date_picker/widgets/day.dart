import 'package:flutter/material.dart';
import 'package:mdspace/presentation/app/resources/font.dart';
import 'package:mdspace/presentation/app/resources/palette.dart';

const double kWidgetHeight = 60.0;

class DayWidget extends StatelessWidget {
  final DateTime date;
  final bool isPastDay;
  final bool isCurrent;
  final bool isSelected;
  final void Function() onTap;
  final int index;
  final bool firstWeekDay;
  final bool lastWeekDay;

  const DayWidget({
    required this.date,
    required this.isCurrent,
    required this.isSelected,
    required this.onTap,
    required this.index,
    required this.firstWeekDay,
    required this.lastWeekDay,
    required this.isPastDay,
  });

  List<Widget> _buildLayout(BuildContext context) {
    final List<Widget> layout = [];

    if (isSelected) {
      layout.add(Center(
        child: Container(
          width: 40,
          height: 40,
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
            color: Palette.blue,
          ),
        ),
      ));
    }

    layout.add(Center(
      child: Stack(
        alignment: AlignmentDirectional.center,
        children: [
          Text(
            date.day.toString(),
            style: isPastDay
                ? const Font.w600(size: 15, color: Palette.grey)
                : Font.w600(size: 15, color: isSelected ? Palette.white : Palette.greyDarkest),
          ),
          if (isPastDay)
            const SizedBox(
              width: 26,
              child: Divider(color: Palette.grey, thickness: 1.5),
            ),
        ],
      ),
    ));

    return layout;
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: SizedBox(
        height: kWidgetHeight,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: kWidgetHeight,
              child: InkWell(
                customBorder: const CircleBorder(),
                splashColor: Palette.white.withOpacity(0.2),
                onTap: isPastDay ? null : onTap,
                child: Stack(
                  children: _buildLayout(context),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
