import 'dart:math';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mdspace/presentation/app/resources/font.dart';
import 'package:mdspace/presentation/app/resources/palette.dart';
import 'package:mdspace/presentation/ui/pages/date_picker/widgets/month.dart';
import 'package:mdspace/presentation/ui/widgets/buttons/custom_elevated_button.dart';

const double _kBottomWidgetHeight = 68.0;

class DatePickerScreenArguments {
  final DateTime? selectedDate;

  DatePickerScreenArguments({required this.selectedDate});
}

class DatePickerScreen extends StatefulWidget {
  static const route = '/DatePickerScreen';

  static PageRoute<dynamic> createRoute(RouteSettings settings) {
    final DatePickerScreenArguments? arg = settings.arguments as DatePickerScreenArguments?;

    return MaterialPageRoute(
        settings: settings,
        fullscreenDialog: true,
        builder: (_) {
          return DatePickerScreen(
            selectedDate: arg!.selectedDate,
          );
        });
  }

  final DateTime? selectedDate;

  const DatePickerScreen({Key? key, this.selectedDate}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _DatePickerScreenState();
}

class _DatePickerScreenState extends State<DatePickerScreen> {
  DateTime? selectedDate;

  late DateTime today;

  @override
  void initState() {
    super.initState();
    today = DateTime.now().emitHHmmSs;
    selectedDate = widget.selectedDate;
  }

  @override
  Widget build(BuildContext context) {
    final EdgeInsets screenPadding = MediaQuery.of(context).padding;

    final double bottomPadding = max(screenPadding.bottom, 16.0);
    return Scaffold(
      backgroundColor: Palette.white,
      body: Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              SizedBox(
                height: 64,
                child: Padding(
                  padding: const EdgeInsets.only(left: 13, right: 13, top: 7),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
                        .map((e) => SizedBox(
                          width: 30,
                          child: Center(
                            child: Text(
                              e,
                              style: const Font.w600(size: 13, color: Palette.grey),
                            ),
                          ),
                        ),
                      ).toList(),
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(left: 22, right: 22),
                child: Divider(color: Palette.grey, height: 1.5),
              ),
              Expanded(
                child: ListView.builder(
                  padding: EdgeInsets.only(bottom: screenPadding.bottom + _kBottomWidgetHeight),
                  itemCount: 13,
                  itemBuilder: (context, index) {
                    final startDate = today;
                    final int year = startDate.year + (index + today.month) ~/ 12;
                    final int month = (index + today.month) % 12;

                    return Padding(
                      padding: const EdgeInsets.only(left: 13, right: 13),
                      child: MonthWidget(
                        date: DateTime(year, month),
                        current: today,
                        selected: selectedDate,
                        onDayClicked: (DateTime date) {
                          setState(() {
                            selectedDate = date;
                          });
                        },
                      ),
                    );
                  },
                ),
              ),
            ],
          ),

          if (selectedDate != null)
            Align(
              alignment: Alignment.bottomCenter,
              child: SizedBox(
                height: _kBottomWidgetHeight + bottomPadding + 1,
                child: Container(
                  color: Palette.white,
                  child: Column(
                    children: [
                      const Divider(color: Palette.grey, height: 1.5),
                      const SizedBox(height: 16),
                      Row(
                        children: [
                          const SizedBox(width: 22),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                'Selected date:',
                                style: Font.w400(
                                  size: 15,
                                  color: Palette.greyDarkest,
                                  height: 1.466,
                                )
                              ),
                              Text(
                                selectedDate != null ? DateFormat('MMM dd, yyyy').format(selectedDate!) : '',
                                style: const Font.w600(
                                  size: 15,
                                  color: Palette.greyDarkest,
                                  height: 1.466,
                                ),
                              ),
                            ],
                          ),
                          const Spacer(),
                          SizedBox(
                            width: 151,
                            height: 48,
                            child: CustomElevatedButton(
                              onPressed: onConfirmClicked,
                              child: const Text(
                                'Confirm',
                                style: Font.w600(
                                  size: 16,
                                  color: Palette.white,
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(width: 22),
                        ],
                      ),
                      SizedBox(height: bottomPadding),
                    ],
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }

  void onConfirmClicked() {
    Navigator.of(context).pop(selectedDate);
  }
}
