import 'package:flutter/material.dart';
import 'package:mdspace/presentation/app/resources/app_scope.dart';
import 'package:mdspace/presentation/ui/pages/auth/sign_in/screen.dart';
import 'package:mdspace/presentation/ui/pages/auth/sign_in_strategy/screen.dart';

class SplashScreen extends StatefulWidget {
  static const route = '/SplashScreen';

  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  ApplicationContextState? appState;

  // AuthBloc get authBloc => appState!.authBloc;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final ApplicationContextState? appState = ApplicationContext.of(context);
    if (this.appState != appState) {
      this.appState = appState;
      // authBloc.initAuthSession();
    }
  }

  @override
  Widget build(BuildContext context) {

    Future.delayed(const Duration(seconds: 1), () =>
        ApplicationContext.of(context)?.navigator?.pushReplacementNamed(SignInStrategyScreen.route),
    );

    return const Scaffold(
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
