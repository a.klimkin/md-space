// import 'package:flutter/material.dart';
// import 'package:mdspace/presentation/app/resources/app_scope.dart';
// import 'package:mdspace/presentation/ui/widgets/items/transformed_flore_view.dart';
// import 'package:provider/provider.dart';
//
// import '../../../../../domain/bloc/sign_in_bloc.dart';
// import '../../../../../domain/repository/auth_repository.dart';
// import '../../../../../domain/repository/user_repository.dart';
//
// class HomeScreen extends StatefulWidget {
//   static const route = '/HomeScreen';
//
//   static PageRoute<dynamic> createRoute(RouteSettings settings) {
//     return MaterialPageRoute<Provider<SignInBloc>>(
//         settings: settings,
//         builder: (BuildContext context) {
//           return Provider<SignInBloc>(
//             create: (_) => SignInBloc(
//               userRepository: userRepository,
//               authRepository: authRepository,
//             ),
//             dispose: (_, bloc) => bloc.dispose(),
//             child: const HomeScreen(),
//           );
//         });
//   }
//
//   const HomeScreen({Key? key}) : super(key: key);
//
//   @override
//   _HomeScreenState createState() => _HomeScreenState();
// }
//
// class _HomeScreenState extends State<HomeScreen> {
//
//   final FocusNode emailFocusNode = FocusNode();
//   final FocusNode passwordFocusNode = FocusNode();
//
//   @override
//   Widget build(BuildContext context) {
//     final SignInBloc bloc = Provider.of<SignInBloc>(context, listen: false);
//
//     return Scaffold(
//       body: SizedBox(
//             height: MediaQuery.of(context).size.height - kToolbarHeight,
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 const Spacer(),
//                 Center(
//                   child: Text(
//                     'FloorUtil.getTitleByFloor(selectedFloorTag)',
//                     style: Theme.of(context).textTheme.headline6,
//                   ),
//                 ),
//                 const Spacer(),
//                 Stack(
//                     children: Floors.values.map((floorTag) => FloorTransformView(
//                       isFocused: floorTag == selectedFloorTag,
//                       spaceFromTop: (200.0 - FloorUtil.getFloorNumber(floorTag) * 100.0),
//                     )).toList()
//                 ),
//                 Transform.translate(
//                   offset: Offset(24, -200),
//                   child: Column(
//                       verticalDirection: VerticalDirection.up,
//                       children: Floors.values.map((floorTag) => Column(
//                         children: [
//                           Padding(
//                             padding: const EdgeInsets.only(bottom: 52.0),
//                             child: RoundFloorButton(
//                               isFocused: floorTag == selectedFloorTag,
//                               label: FloorUtil.getFloorNumber(floorTag).toString(),
//                               onPressed: () {
//                                 BlocProvider.of<MainPageBloc>(context).add(
//                                   FloorSelected(
//                                     context: context,
//                                     floorTag: floorTag,
//                                   ),
//                                 );
//                               },
//                             ),
//                           ),
//                         ],
//                       )).toList()
//                   ),
//                 ),
//                 Center(
//                   child: selectedFloorTag != null ? PrimaryButton(
//                     label: "Show the plan",
//                     onPressed: () {
//                       BlocProvider.of<MainPageBloc>(context).add(
//                         WentToSelectedFloor(context),
//                       );
//                     },
//                   ) : SizedBox(height: kButtonHeight),
//                 ),
//                 Spacer(
//                   flex: 2,
//                 ),
//               ],
//             ),
//           ),
//         ),
//       ),
//     ),
//     );
//   }
// }
