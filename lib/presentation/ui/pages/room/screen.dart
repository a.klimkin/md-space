import 'package:flutter/material.dart';
import 'package:mdspace/presentation/app/resources/app_scope.dart';
import 'package:mdspace/presentation/app/resources/font.dart';
import 'package:mdspace/presentation/app/resources/palette.dart';
import 'package:mdspace/presentation/ui/pages/date_picker/screen.dart';
import 'package:mdspace/presentation/ui/widgets/buttons/custom_elevated_button.dart';
import 'package:mdspace/presentation/ui/widgets/state_listener/widget.dart';
import 'package:provider/provider.dart';

import '../../../../domain/bloc/work_place_bloc.dart';
import '../../../../domain/repository/table_repository.dart';
import '../../../../model/room_model.dart';
import '../../../app/resources/constants.dart';
import '../../widgets/items/room_view.dart';
import '../../widgets/table_info/table_info_card.dart';

class RoomScreenArguments {
  final RoomModel roomModel;

  RoomScreenArguments({
    required this.roomModel,
  });
}

class RoomScreen extends StatefulWidget {
  static const route = '/RoomScreen';

  static PageRoute<dynamic> createRoute(
      RouteSettings settings, {
        required TableRepository repository,
      }) {
    final RoomScreenArguments? arg = settings.arguments as RoomScreenArguments?;
    return MaterialPageRoute<Provider<WorkPlaceBloc>>(
        settings: settings,
        builder: (BuildContext context) {
          return Provider<WorkPlaceBloc>(
            create: (_) => WorkPlaceBloc(
              repository: repository,
              room: arg!.roomModel,
            ),
            dispose: (_, bloc) => bloc.dispose(),
            child: const RoomScreen(),
          );
        });
  }

  const RoomScreen({Key? key}) : super(key: key);

  @override
  _RoomScreenState createState() => _RoomScreenState();
}

class _RoomScreenState extends State<RoomScreen> {

  
  @override
  void initState() {
    super.initState();
    Provider.of<WorkPlaceBloc>(context, listen: false).uploadTables();
  }

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<WorkPlaceBloc>(context, listen: false);
    return Scaffold(
      body: StreamListener<PlaceState>(
        stream: bloc.outStream,
        onValueReceived: (state) {
          if (state.status == PlaceStatus.bookingSuccess) {
            bloc.resetTable();

            // showBottomSheet(context: context, builder: builder)
            showDialog(
              context: context,
              builder: (context) {
                return Container(
                  margin: EdgeInsets.only(top: 200, bottom: 300, left: 48, right: 48),
                  width: 240,
                  decoration: BoxDecoration(
                    color: Palette.white,
                    borderRadius: BorderRadius.circular(24),
                  ),
                  padding: const EdgeInsets.all(12),
                  child: Center(
                    child: Column(
                      children: [
                        const Icon(Icons.check_circle_rounded, color: Palette.green, size: 56),
                        const SizedBox(height: 24),
                        const Text(
                          'Your booking is success',
                          style: Font.w500(),
                        ),
                        const SizedBox(height: 24),
                        CustomElevatedButton(
                          onPressed: Navigator.of(context).pop,
                          child: const Text(
                            'OK',
                            style: Font.w500(size: 16, color: Palette.white),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              },
            );

          }
        },
        child: StreamBuilder<PlaceState>(
          initialData: bloc.currentState,
          stream: bloc.outStream,
          builder: (context, snapshot) {
            return GestureDetector(
              onTap: () {
                bloc.resetTable();
              },
              child: Stack(
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                      top: MediaQuery.of(context).padding.top + 48,
                      bottom: MediaQuery.of(context).padding.bottom + 24,
                    ),
                    child: InteractiveViewer(
                      child: Center(
                        child: RoomWidget(
                          onSelectTable: bloc.selectTable,
                          room: bloc.room,
                          selectedTable: snapshot.data!.selectedPlace,
                          tables: snapshot.data!.tables,
                        ),
                      ),
                    ),
                  ),
                  if (bloc.isTableSelected)
                    Positioned(
                      bottom: 0,
                      child: TableInfoCard(
                        table: snapshot.data!.selectedPlace!,
                        selectedTime: snapshot.data!.selectedDate,
                        onBookPressed: () {
                          bloc.bookTable();
                        },
                        onChangeDatePressed: () {
                          showSelectDate(context);
                        },
                      ),
                    ),
                ],
              ),
            );
          }
        ),
      ),
    );
  }

  Future<void> showSelectDate(BuildContext context) async {
    final bloc = Provider.of<WorkPlaceBloc>(context, listen: false);

    final dateTime = bloc.currentState.selectedDate;
    final dynamic selectedDate = await Navigator.of(context).pushNamed(
      DatePickerScreen.route,
      arguments: DatePickerScreenArguments(selectedDate: dateTime),
    );
    if (selectedDate != null) {
      bloc.setDate(selectedDate as DateTime);
    }
  }
}
