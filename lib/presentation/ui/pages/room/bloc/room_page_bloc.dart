// import 'dart:async';
//
// import 'package:bloc/bloc.dart';
// import 'package:equatable/equatable.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:table_booking/data/entity/entities.dart';
// import 'package:table_booking/domain/use_cases/use_cases.dart';
// import 'package:table_booking/presentation/ui/dialogs/date_booking_dialog.dart';
//
// part 'room_page_event.dart';
// part 'room_page_state.dart';
//
// class RoomPageBloc extends Bloc<RoomPageEvent, RoomPageState> {
//   RoomPageBloc() : super(RoomPageInitial());
//
//   UseCases _useCases = UseCases();
//
//   @override
//   Stream<RoomPageState> mapEventToState(
//     RoomPageEvent event,
//   ) async* {
//     if (event is InitRoom) {
//       List<RoomModel> rooms = await _useCases.roomManager.getRooms();
//
//       yield SetRoomData(rooms.first);
//
//       List<WorkPlace> tables = await _useCases.tableManager.getTables();
//
//       yield SetTablesData(tables);
//     } else if (event is SelectedTable) {
//       yield ShowTableInfo(event.table);
//     } else if (event is BookPressed) {
//       if (event.range != null) {
//         _useCases.tableManager.bookTable(event.table.id, event.range);
//       }
//     }
//   }
// }
