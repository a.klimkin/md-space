// part of 'room_page_bloc.dart';
//
// abstract class RoomPageEvent extends Equatable {
//   final BuildContext context;
//   const RoomPageEvent(this.context);
//
//   @override
//   List<Object> get props => [];
// }
//
// class InitRoom extends RoomPageEvent {
//   InitRoom(BuildContext context) : super(context);
// }
//
// class SelectedTable extends RoomPageEvent {
//   final WorkPlace table;
//   SelectedTable({BuildContext context, this.table}) : super(context);
//
//   @override
//   List<Object> get props => [table];
// }
//
// class PressedAddItem extends RoomPageEvent {
//   PressedAddItem(BuildContext context) : super(context);
// }
//
// class RequestCreated extends RoomPageEvent {
//   RequestCreated(BuildContext context) : super(context);
// }
//
// class BookPressed extends RoomPageEvent {
//   final DateTimeRange range;
//   final WorkPlace table;
//
//   BookPressed({BuildContext context, this.table, this.range}) : super(context);
//
//   @override
//   List<Object> get props => [table, range];
// }
//
// class EditPressed extends RoomPageEvent {
//   EditPressed(BuildContext context) : super(context);
// }