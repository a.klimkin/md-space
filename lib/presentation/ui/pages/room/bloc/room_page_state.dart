// part of 'room_page_bloc.dart';
//
// abstract class RoomPageState extends Equatable {
//   const RoomPageState();
//
//   @override
//   List<Object> get props => [];
// }
//
// class RoomPageInitial extends RoomPageState {}
//
// class SetRoomData extends RoomPageState {
//   final RoomModel room;
//
//   SetRoomData(this.room);
//
//   @override
//   List<Object> get props => [room];
// }
//
// class SetTablesData extends RoomPageState {
//   final List<WorkPlace> tables;
//
//   SetTablesData(this.tables);
//
//   @override
//   List<Object> get props => [tables];
// }
//
// class ShowTableInfo extends RoomPageState {
//   final WorkPlace table;
//
//   ShowTableInfo(this.table);
//
//   @override
//   List<Object> get props => [table];
// }