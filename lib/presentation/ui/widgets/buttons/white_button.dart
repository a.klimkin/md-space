// import 'package:flutter/material.dart';
// import 'package:table_booking/presentation/app/resources/resources.dart';
//
// class WhiteButton extends StatelessWidget {
//   final String label;
//   final VoidCallback onPressed;
//
//   const WhiteButton({
//     Key key,
//     @required this.label,
//     @required this.onPressed,
//   }) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return ElevatedButtonTheme(
//       data: ElevatedButtonThemeData(
//         style: ButtonStyle(
//             animationDuration: Duration(milliseconds: 200),
//             backgroundColor: MaterialStateProperty.all<Color>(Theme.of(context).backgroundColor),
//             elevation: MaterialStateProperty.resolveWith<double>((states) {
//               double elevation = 6.0;
//
//               if (states.contains(MaterialState.disabled))
//                 return 0;
//               if (states.contains(MaterialState.hovered))
//                 return elevation + 2;
//               if (states.contains(MaterialState.focused))
//                 return elevation + 2;
//               if (states.contains(MaterialState.pressed))
//                 return elevation - 3;
//               return elevation;
//             }),
//             shape: MaterialStateProperty.all<OutlinedBorder>(
//               RoundedRectangleBorder(
//                 borderRadius: BorderRadius.circular(kButtonHeight),
//               ),
//             ),
//         ),
//       ),
//       child: Container(
//         height: kButtonHeight,
//         width: kButtonWidth,
//         child: ElevatedButton(
//           onPressed: onPressed,
//           child: Text(
//             label,
//             style: Theme.of(context).textTheme.button.copyWith(
//               color: Theme.of(context).primaryColor,
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }
