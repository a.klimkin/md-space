// import 'package:flutter/material.dart';
// import 'package:table_booking/presentation/app/resources/resources.dart';
//
// class RoundFloorButton extends StatelessWidget {
//   final VoidCallback onPressed;
//   final bool isFocused;
//   final String label;
//   final double size;
//   final Color color;
//   final focusedColor;
//
//   const RoundFloorButton({
//     Key key,
//     @required this.onPressed,
//     @required this.isFocused,
//     @required this.label,
//     this.size,
//     this.color,
//     this.focusedColor,
//   }) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     var buttonHeight = size == null ? kButtonHeight : size;
//     var backgroundColor = color ?? Theme.of(context).backgroundColor;
//     var backgroundFocusedColor = focusedColor ?? Theme.of(context).primaryColor;
//
//     return ElevatedButtonTheme(
//       data: ElevatedButtonThemeData(
//         style: ButtonStyle(
//           animationDuration: Duration(milliseconds: 200),
//           overlayColor: isFocused ? null : MaterialStateProperty.all<Color>(backgroundFocusedColor.withOpacity(0.2)),
//           backgroundColor: isFocused
//               ? MaterialStateProperty.all<Color>(backgroundFocusedColor)
//               : MaterialStateProperty.all<Color>(backgroundColor) ,
//           elevation: MaterialStateProperty.resolveWith<double>((states) {
//             double elevation = 6.0;
//
//             if (states.contains(MaterialState.disabled))
//               return 0;
//             if (states.contains(MaterialState.hovered))
//               return elevation + 2;
//             if (states.contains(MaterialState.focused))
//               return elevation + 2;
//             if (states.contains(MaterialState.pressed))
//               return elevation - 3;
//             return elevation;
//           }),
//           shape: MaterialStateProperty.all<OutlinedBorder>(
//             RoundedRectangleBorder(
//               borderRadius: BorderRadius.circular(buttonHeight),
//             ),
//           ),
//         ),
//       ),
//       child: Container(
//         height: buttonHeight,
//         width: buttonHeight,
//         child: ElevatedButton(
//           onPressed: onPressed,
//           child: Text(
//             label,
//             style: Theme.of(context).textTheme.button.copyWith(
//               color: isFocused ? backgroundColor : backgroundFocusedColor,
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }