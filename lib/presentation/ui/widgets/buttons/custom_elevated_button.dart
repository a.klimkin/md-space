import 'package:flutter/material.dart';
import 'package:mdspace/presentation/app/resources/palette.dart';

import '../../../app/resources/constants.dart';

class CustomElevatedButton extends StatelessWidget {
  final VoidCallback onPressed;
  final Widget child;
  final Color? color;
  final Color? disableColor;

  const CustomElevatedButton({
    Key? key,
    required this.onPressed,
    required this.child,
    this.color, 
    this.disableColor, 
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButtonTheme(
      data: ElevatedButtonThemeData(
        style: ButtonStyle(
          animationDuration: const Duration(milliseconds: 200),
          backgroundColor: MaterialStateProperty.resolveWith<Color>((states) {
            if (states.contains(MaterialState.disabled)) return disableColor ?? Palette.grey;
            return color ?? Palette.green;
          }),
          elevation: MaterialStateProperty.resolveWith<double>((states) {
            const double elevation = 6.0;

            if (states.contains(MaterialState.disabled)) return 0;
            if (states.contains(MaterialState.hovered)) return elevation + 2;
            if (states.contains(MaterialState.focused)) return elevation + 2;
            if (states.contains(MaterialState.pressed)) return elevation - 3;
            return elevation;
          }),
          shape: MaterialStateProperty.all<OutlinedBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(Constants.kButtonHeight),
            ),
          ),
        ),
      ),
      child: SizedBox(
        height: Constants.kButtonHeight,
        width: Constants.kButtonWidth,
        child: ElevatedButton(
          onPressed: onPressed.call,
          child: Center(
            child: child,
          ),
        ),
      ),
    );
  }
}
