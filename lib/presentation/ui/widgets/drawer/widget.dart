import 'package:flutter/material.dart';

class CustomDrawer extends StatefulWidget {
  final bool showDrawer;
  final Widget drawer;
  final Widget child;

  const CustomDrawer({
    Key? key,
    required this.showDrawer,
    required this.drawer,
    required this.child,
  }) : super(key: key);

  @override
  _CustomDrawerState createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<Offset> _animationOffset;

  bool _overlayVisible = false;

  _CustomDrawerState();

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(vsync: this, duration: const Duration(milliseconds: 300));
    
    if (widget.showDrawer) {
      _controller.forward();
    }
  }

  @override
  void didUpdateWidget(CustomDrawer oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (!oldWidget.showDrawer && widget.showDrawer) {
      _controller.forward();
    }

    if (oldWidget.showDrawer && !widget.showDrawer) {
      _controller.reverse();
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    final screenSize = MediaQuery.of(context).size;

    final endX = (screenSize.width - 240.0)/screenSize.width;

    _animationOffset = Tween<Offset>(
        begin: const Offset(1.0, 0.0),
        end: Offset(endX, 0.0)
    ).animate(_controller);

    // todo replace with builder
    _animationOffset.addStatusListener((status) {
      if (status == AnimationStatus.forward) setState(() => {_overlayVisible = true});
      if (status == AnimationStatus.dismissed) setState(() => {_overlayVisible = false});
    });

    final widgets = <Widget>[];
    widgets.add(widget.child);

    if (_overlayVisible == true) {
      final modal = SlideTransition(
        position: _animationOffset,
        child: SizedBox(
          height: screenSize.height,
          width: screenSize.width,
          child: Stack(
            children: [widget.drawer],
          ),
        ),
      );

      widgets.add(modal);
    }

    return Stack(children: widgets);
  }
}
