import 'dart:math';

import 'package:flutter/material.dart';

import '../../../app/resources/palette.dart';

class FloorTransformView extends StatelessWidget {
  final Color disableColor;
  final Color focusedColor;
  final Size? disableSize;
  final double spaceFromTop;
  final bool isFocused;

  const FloorTransformView({
    Key? key,
    required this.isFocused,
    this.disableColor = Palette.grey_50,
    this.focusedColor = Palette.blue_50,
    this.spaceFromTop = 0.0,
    this.disableSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final halfScreen = MediaQuery.of(context).size.width / 2;

    final disableS = disableSize ?? Size(halfScreen, halfScreen);

    return Transform.translate(
      offset: Offset(halfScreen * 1.2, spaceFromTop),
      child: Transform(
        transform: Matrix4.rotationX(60 * pi / 180),
        child: Container(
          transform: Matrix4.rotationZ(45 * pi / 180),
          height: disableS.height,
          width: disableS.width,
          decoration: BoxDecoration(
            color: isFocused ? focusedColor : disableColor,
            boxShadow: isFocused
              ? [
                  BoxShadow(
                    color: focusedColor,
                    blurRadius: 12,
                    spreadRadius: 6,
                  )
                ]
              : null,
          ),
        ),
      ),
    );
  }
}
