import 'package:flutter/material.dart';

import '../../../../model/room_model.dart';
import '../../../../model/table_model.dart';
import '../../../app/resources/constants.dart';
import '../../../app/resources/palette.dart';
import 'render_work_space.dart';

class RoomWidget extends StatelessWidget {
  final RoomModel room;
  final TableModel? selectedTable;
  final List<TableModel> tables;
  final void Function(TableModel table) onSelectTable;
  const RoomWidget({
    Key? key,
    required this.room,
    required this.tables,
    required this.onSelectTable,
    required this.selectedTable,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    final coefficient = _getRenderCoefficient(context, size: room.size);

    return Container(
      width: coefficient * room.size.width,
      height: coefficient * room.size.height,
      decoration: BoxDecoration(
        color: Palette.blue,
        border: Border.all(
          color: Palette.greyDarkest,
          width: 3,
        ),
      ),
      child: Stack(
        children: tables.map((table) =>
            RenderWorkPlace(
              isSelected: selectedTable == table,
              coefficient: coefficient,
              table: table,
                onSelectTable: onSelectTable,
            )).toList(),
      ),
    );
  }

  double _getRenderCoefficient(BuildContext context, {Size size = const Size(1, 1)}) {
    final screenSize = MediaQuery.of(context).size;
    final workPlaceWidth = screenSize.width - Constants.k24 * 2;
    final workPlaceHeight = screenSize.height - Constants.k24 * 2 - Constants.kAppBarHeight;
    
    var coefficient = workPlaceWidth / size.width;
    
    if (coefficient * size.height > workPlaceHeight) {
      coefficient = workPlaceHeight / size.height;
    }
    return coefficient;
  }
}
