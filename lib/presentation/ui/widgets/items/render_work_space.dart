import 'dart:math';

import 'package:flutter/material.dart';

import '../../../../model/table_model.dart';
import '../../../app/resources/palette.dart';

class RenderWorkPlace extends StatelessWidget {
  final double coefficient;
  final TableModel table;
  final void Function(TableModel table) onSelectTable;
  final bool isSelected;

  const RenderWorkPlace({
    Key? key,
    required this.coefficient,
    required this.table,
    required this.onSelectTable,
    required this.isSelected,

  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        left: coefficient * table.position.dx,
        top: coefficient * table.position.dy,
      ),
      child: GestureDetector(
        onTap: () => onSelectTable(table),
        child: Container(
          transform: Matrix4.rotationZ(0 * pi / 180),
          width: table.size.width * coefficient,
          height: table.size.height * coefficient,
          decoration: BoxDecoration(
            color: isSelected ? Palette.green : Palette.grey,
            border: Border.all(color: Palette.greyDarkest),
          ),
        ),
      )
    );
  }
}
