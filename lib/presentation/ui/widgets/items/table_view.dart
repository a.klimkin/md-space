import 'dart:math';

import 'package:flutter/material.dart';

import '../../../../model/table_model.dart';
import '../../../app/resources/palette.dart';

class TableWidget extends StatelessWidget {
  final VoidCallback onPressed;
  final double sizeCoefficient;
  final bool isSelected;
  final TableModel table;

  const TableWidget({
    Key? key,
    required this.table,
    required this.sizeCoefficient,
    required this.onPressed,
    required this.isSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        transform: Matrix4.rotationZ(0 * pi / 180),
        width: table.size.width * sizeCoefficient,
        height: table.size.height * sizeCoefficient,
        decoration: BoxDecoration(
          color: isSelected ? Palette.green : Palette.grey,
          border: Border.all(color: Palette.greyDarkest),
        ),
      ),
    );
  }
}
