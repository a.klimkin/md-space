// import 'package:flutter/material.dart';
// import 'package:table_booking/data/entity/entities.dart';
// import 'package:table_booking/domain/use_cases/use_cases.dart';
//
// class ItemRows extends StatelessWidget {
//   final List<String> itemIds;
//
//   ItemRows({Key key, this.itemIds}) : super(key: key);
//
//   final UseCases _useCases = UseCases();
//
//   @override
//   Widget build(BuildContext context) {
//     return (itemIds == null || itemIds.isEmpty)
//         ? Text(
//       "No Items",
//       style: Theme.of(context).textTheme.bodyText2,
//     )
//         : Container(
//       width: double.infinity,
//       alignment: Alignment.centerLeft,
//       padding: EdgeInsets.only(left: 16.0),
//       child: Column(
//         children: itemIds
//             .map(
//               (id) => FutureBuilder<ItemModel>(
//                 future: _useCases.itemManager.getItem(id),
//                 builder: (context, snapshot) {
//                   if (snapshot.hasData) {
//                     return Text(
//                       '${itemIds.indexOf(id) + 1}. ${snapshot.data.type}, ${snapshot.data.name}',
//                       style: Theme.of(context).textTheme.bodyText2,
//                     );
//                   } else {
//                     return Text(
//                       ' ',
//                       style: Theme.of(context).textTheme.bodyText2,
//                     );
//                   }
//
//                 },
//               )
//         )
//             .toList(),
//       ),
//     );
//   }
// }