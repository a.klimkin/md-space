import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../../model/table_model.dart';
import '../../../app/resources/constants.dart';
import '../../../app/resources/font.dart';
import '../../../app/resources/palette.dart';
import '../buttons/custom_elevated_button.dart';

class TableInfoCard extends StatefulWidget {
  final TableModel table;
  final VoidCallback onChangeDatePressed;
  final VoidCallback onBookPressed;
  final DateTime? selectedTime;

  const TableInfoCard({
    Key? key,
    required this.table,
    required this.onChangeDatePressed,
    required this.onBookPressed,
    this.selectedTime,
  }) : super(key: key);

  @override
  _TableInfoCardState createState() => _TableInfoCardState();
}

class _TableInfoCardState extends State<TableInfoCard> with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(Constants.k24),
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(Constants.k24),
          topRight: Radius.circular(Constants.k24),
        ),
        boxShadow: [
          BoxShadow(
            color: Theme.of(context).dividerColor.withOpacity(0.2),
            blurRadius: 12,
            offset: const Offset(0, 6),
          ),
        ],
      ),
      child: Column(
        children: [
          Center(
            child: Text(
              "Work Place: ${widget.table.id}",
              style: const Font.w500(size: 16, color: Palette.greyDarkest, height: 1.2),
            ),
          ),
          const SizedBox(height: Constants.k24),
          if (widget.selectedTime != null) ...[
            Center(
              child: Text(
                "Selected date: ${DateFormat('MMM dd, yyyy').format(widget.selectedTime!)}",
                style: const Font.w400(color: Palette.greyDarkest, height: 1.2),
              ),
            ),
            const SizedBox(height: Constants.k24),
          ],
          CustomElevatedButton(
            onPressed: widget.onChangeDatePressed,
            color: Palette.white,
            child: const Text(
              'Select date',
              style: Font.w500(size: 16, color: Palette.greyDarkest),
            ),
          ),
          const SizedBox(height: Constants.k24),
          CustomElevatedButton(
            onPressed: widget.onBookPressed,
            child: const Text(
              'Book it',
              style: Font.w500(size: 16, color: Palette.white),
            ),
          ),
        ],
      ),
    );
  }
}
