// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:flutter/material.dart';
//
// class UserInfoWidget extends StatelessWidget {
//
//   @override
//   Widget build(BuildContext context) {
//
//     final user = UseCases().userManager.getCurrentUser();
//
//     return GestureDetector(
//       onHorizontalDragUpdate: (details) {
//         if (details.delta.dx > 0) {
//           BlocProvider.of<BaseBloc>(context)
//               .add(DrawerHided(context));
//         }
//       },
//       child: Container(
//         width: 240.0,
//         height: MediaQuery.of(context).size.height,
//         color: Theme.of(context).backgroundColor,
//         padding: EdgeInsets.all(kMainPadding),
//         child: user == null ? NoAuthUser() : AuthUser(user: user),
//       ),
//     );
//   }
// }
//
// class NoAuthUser extends StatelessWidget {
//   const NoAuthUser({
//     Key key,
//   }) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//
//     var image = NetworkImage("https://hpk.edu.ua/uploads/2015/03/user-icon-silhouette.png");
//
//     return Column(
//       children: [
//         Padding(
//           padding: const EdgeInsets.only(top: 56.0),
//           child: CircleAvatar(
//             radius: 30,
//             backgroundImage: image,
//           ),
//         ),
//         TextButton(
//           onPressed: () {
//             BlocProvider.of<BaseBloc>(context)
//                 .add(SignInPressed(context));
//           },
//           child: Text(
//             "Sign in",
//           ),
//         ),
//         TextButton(
//           onPressed: () {
//             BlocProvider.of<BaseBloc>(context)
//                 .add(RegistrationPressed(context));
//           },
//           child: Text(
//             "Registration",
//           ),
//         ),
//       ],
//     );
//   }
// }
//
// class AuthUser extends StatelessWidget {
//   const AuthUser({
//     Key key,
//     this.user,
//   }) : super(key: key);
//
//   final User user;
//
//   @override
//   Widget build(BuildContext context) {
//
//     print(user);
//
//     return Column(
//       children: [
//         Padding(
//           padding: const EdgeInsets.only(top: 56.0),
//           child: CircleAvatar(
//             radius: 30,
//             backgroundImage: NetworkImage(user.photoURL ?? "https://hpk.edu.ua/uploads/2015/03/user-icon-silhouette.png"),
//           ),
//         ),
//         TextButton(
//           child: Icon(
//             Icons.edit,
//           ),
//           onPressed: () {
//             BlocProvider.of<BaseBloc>(context)
//                 .add(EditProfilePressed(context));
//           },
//         ),
//         Spacer(),
//         Padding(
//           padding: EdgeInsets.only(bottom: kMainPadding),
//           child: TextButton(
//             onPressed: () {
//               BlocProvider.of<BaseBloc>(context)
//                   .add(LogOutPressed(context));
//             },
//             child: Text(
//               "Log Out",
//             ),
//           ),
//         ),
//       ],
//     );
//   }
// }
//
