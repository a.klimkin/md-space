// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
//
// class CustomTextField extends StatelessWidget {
//   final TextEditingController controller;
//   final String label;
//   final bool obscureText;
//   final bool hasError;
//   final int maxLines;
//   final void Function() onTap;
//   final Widget suffixIcon;
//   final List<TextInputFormatter> inputFormatters;
//   final TextInputType keyboardType;
//   final String Function(String value) validator;
//   final void Function(String value) onChanged;
//   final void Function() onEditingComplete;
//
//   const CustomTextField({
//     Key? key,
//     required this.label,
//     this.controller,
//     this.keyboardType,
//     this.maxLines = 1,
//     this.obscureText = false,
//     this.hasError = false,
//     this.onChanged,
//     this.validator,
//     this.suffixIcon,
//     this.onEditingComplete,
//     this.onTap,
//     this.inputFormatters,
//   }) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//
//     return Padding(
//       padding: const EdgeInsets.only(bottom: 12),
//       child: SizedBox(
//         width: double.infinity,
//         height: maxLines == 1 ? kButtonHeight : null,
//         child: TextFormField(
//           onTap: onTap,
//           minLines: 1,
//           maxLines: maxLines,
//           obscureText: obscureText,
//           controller: controller,
//           style: Theme.of(context).textTheme.bodyText2,
//           inputFormatters: inputFormatters,
//           keyboardType: keyboardType,
//           validator: validator,
//           onChanged: onChanged,
//           onEditingComplete: onEditingComplete,
//           decoration: InputDecoration(
//             hintMaxLines: maxLines,
//             suffixIcon: suffixIcon,
//             hintText: label,
//             hintStyle: Theme.of(context).textTheme.bodyText2.copyWith(
//               color: Theme.of(context).dividerColor.withOpacity(0.5),
//               height: 0.9,
//             ),
//             errorText: hasError ? "" : null,
//             errorStyle: TextStyle(fontSize: 0.1, height: 0),
//             contentPadding: EdgeInsets.symmetric(horizontal: kMainPadding, vertical: 16.0),
//             focusedBorder: OutlineInputBorder(
//                 borderRadius: BorderRadius.circular(kMainPadding),
//                 borderSide: BorderSide(color: Theme.of(context).primaryColor)),
//             enabledBorder: OutlineInputBorder(
//                 borderRadius: BorderRadius.circular(kMainPadding),
//                 borderSide: BorderSide(color: Theme.of(context).disabledColor)),
//             border: OutlineInputBorder(
//                 borderRadius: BorderRadius.circular(kMainPadding),
//                 borderSide: BorderSide(color: Theme.of(context).disabledColor)),
//           ),
//         ),
//       ),
//     );
//   }
// }
