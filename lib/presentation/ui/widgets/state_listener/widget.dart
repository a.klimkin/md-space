import 'dart:async';

import 'package:flutter/material.dart';

class StreamListener<S> extends StatefulWidget {
  final Stream<S>? stream;
  final void Function(S state) onValueReceived;
  final Widget child;

  const StreamListener({
    Key? key,
    required this.stream,
    required this.onValueReceived,
    required this.child,
  }) : super(key: key);

  @override
  _StreamListenerState<S> createState() => _StreamListenerState();
}

class _StreamListenerState<S> extends State<StreamListener<S>> {
  StreamSubscription<S>? _subscription;

  @override
  void dispose() {
    _subscription?.cancel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _subscription = widget.stream?.listen(widget.onValueReceived);
  }

  @override
  void didUpdateWidget(covariant StreamListener<S> oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.stream != widget.stream) {
      _subscription?.cancel();
      _subscription = widget.stream?.listen(widget.onValueReceived);
    }
  }

  @override
  Widget build(BuildContext context) => widget.child;
}
