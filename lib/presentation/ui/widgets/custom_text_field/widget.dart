import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../../app/resources/font.dart';
import '../../../app/resources/palette.dart';

class CustomInputField extends StatefulWidget {
  final String hintText;
  final String? labelText;
  final String textValue;
  final TextInputAction inputAction;
  final TextCapitalization textCapitalization;
  final TextInputType keyboardType;
  final FocusNode? focusNode;
  final int maxLength;
  final void Function(String) onTextChanged;
  final void Function(String)? onSubmitted;
  final Stream<String?>? errorStream;
  final bool autofocus;
  final bool obscureText;
  final bool readOnly;
  final bool autocorrect;
  final void Function()? onTap;

  const CustomInputField({
    Key? key,
    required this.textValue,
    required this.hintText,
    required this.onTextChanged,
    required this.errorStream,
    this.onSubmitted,
    this.labelText,
    this.inputAction = TextInputAction.next,
    this.textCapitalization = TextCapitalization.words,
    this.keyboardType = TextInputType.text,
    this.focusNode,
    this.maxLength = TextField.noMaxLength,
    this.autofocus = false,
    this.obscureText = false,
    this.readOnly = false,
    this.autocorrect = true,
    this.onTap,
  }) : super(key: key);

  @override
  CustomInputFieldState createState() => CustomInputFieldState();
}

class CustomInputFieldState extends State<CustomInputField> {
  late TextEditingController controller;

  @override
  void initState() {
    super.initState();
    controller = TextEditingController(text: widget.textValue)
      ..selection = TextSelection.fromPosition(TextPosition(offset: widget.textValue.length));
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  @override
  void didUpdateWidget(covariant CustomInputField oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (controller.text != widget.textValue) {
      controller.text = widget.textValue;
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 56,
      child: StreamBuilder<String?>(
        stream: widget.errorStream,
        builder: (_, snapshot) {
          return TextField(
            autocorrect: widget.autocorrect,
            readOnly: widget.readOnly,
            onTap: widget.onTap,
            cursorColor: const Color(0xFF000000),
            cursorWidth: 1,
            obscureText: widget.obscureText,
            autofocus: widget.autofocus,
            controller: controller,
            textCapitalization: widget.textCapitalization,
            keyboardType: widget.keyboardType,
            textInputAction: widget.inputAction,
            // FocusNode breaks samsung keyboard behavior.
            // https://github.com/flutter/flutter/issues/90525
            focusNode: Platform.isIOS ? widget.focusNode : null,
            decoration: InputDecoration(
              labelText: widget.labelText,
              hintText: widget.hintText,
              hintStyle: const Font.w400(size: 16, color: Palette.greyDarkest_50),
              counterText: '',
              errorText: snapshot.data?.trim(),
              isDense: true,
              errorMaxLines: 2,
              // contentPadding: EdgeInsets.zero,
              // contentPadding: EdgeInsets.symmetric(horizontal: kMainPadding, vertical: 16.0),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(28),
                borderSide: const BorderSide(color: Palette.blue),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(28),
                borderSide: const BorderSide(color: Palette.grey),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(28),
                borderSide: const BorderSide(color: Palette.grey),
              ),
            ),
            style: const Font.w400(size: 16, color: Palette.greyDarkest),
            maxLength: widget.maxLength,
            onChanged: widget.onTextChanged,
            onSubmitted: widget.onSubmitted,
          );
        },
      ),
    );
  }
}
