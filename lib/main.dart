import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:mdspace/presentation/app/resources/dependencies.dart';

import 'presentation/app/app.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();
  await Dependencies.init();

  runApp(const App());
}


